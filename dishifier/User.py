try:
    from cPickle import dump as obj_dump, load as obj_load
except ImportError:
    from pickle import dump as obj_dump, load as obj_load
from os.path import dirname as root_dir
from os.path import join as create_path
from os.path import isfile as file_exists
from json import load as load_tr_data

PROJECT_DIR = root_dir(root_dir(__file__))


class Profile:

    DESTINATION_FILE_NAME = 'users.dat'

    def __init__(self, usr):
        self.id = usr
        self.recommendations = []
        self.personal_details = {
            'first name': '',
            'last name': '',
            'age': 18,
            'gender': None
        }
        self.preferences = {
            'modified': False,
            'courses': {},
            'cuisines': {},
            'allergies': [],
            'diets': [],
            'flavour_attrs': {
                'bitter': 0,
                'meaty': 0,
                'piquant': 0,
                'salty': 0,
                'sour': 0,
                'sweet': 0
            }
        }
        self.favourite = {
            'modified': False,
            'recipes': [],
        }
        self.training = {
            'tutorial-complete': False,
            'complete': False,
            'stage': 0
        }

    def reset_training_data(self):
        self.training['complete'] = False
        self.training['stage'] = 1

    def load_preferences(self, data):

        if data:
            self.preferences['modified'] = True
        if 'flavors' in data:
            for key, val in data['flavors'].iteritems():
                self.preferences['flavour_attrs'][key] = (self.preferences['flavour_attrs'][key] + val) / 2
        if 'attributes' in data:
            if 'course' in data['attributes']:
                for c in data['attributes']['course']:
                    option = self.preferences['courses'].setdefault(c, {})
                    if option:
                        option[c] += 1
                    else:
                        option[c] = 1
            if 'cuisine' in data['attributes']:
                for c in data['attributes']['cuisine']:
                    option = self.preferences['cuisines'].setdefault(c, {})
                    if option:
                        option[c] += 1
                    else:
                        option[c] = 1
        if 'diet' in data:
            self.preferences['diets'].append(data['diet'])
        if 'allergy' in data:
            self.preferences['allergies'].append(data['allergy'])

    def set_recommendations(self, recipes):
        self.recommendations = recipes

    def add_fav_recipe(self, rid):
        if rid not in self.favourite['recipes']:
            self.favourite['modified'] = True
            self.favourite['recipes'].append(rid)

    def remove_fav_recipe(self, uid):
        self.favourite['modified'] = True
        i = self.favourite['recipes'].index(uid)
        del self.favourite['recipes'][i]


def dump_profile(profile):
    file_path = create_path(PROJECT_DIR, 'user_data', '{0}.dat'.format(profile.id))
    with open(file_path, 'wb') as f:
        obj_dump(profile, f)


def load_profile(profile_name):
    obj = None
    file_path = create_path(PROJECT_DIR, 'user_data', '{0}.dat'.format(profile_name))

    if file_exists(file_path):
        with open(file_path, 'rb') as f:
            obj = obj_load(f)
        msg = "Profile Data file found !"
    else:
        msg = "Profile file for user '{0}' does not exist." \
              " Would you want to create one now ?".format(profile_name)

    return obj, msg


def user_login(usr):

    if usr:
        p, msg = load_profile(usr)
        return p, msg

    return None, None


def load_training_data():

    tr_file = create_path(PROJECT_DIR, 'init_tr')

    try:
        with open(tr_file) as f:
            data = load_tr_data(f)
            data['training'] = True
    except IOError as ioe:
        msg = 'Problem with loading training data:\n'
        raise Exception(msg + str(ioe))

    return data
