import requests
from requests.exceptions import ContentDecodingError
from re import compile as re_compile
from Utils import validate_status


class Yummly:

    # STATUS CODES #
    # 200 = OK
    # 302 = REDIRECT
    # 304 = CACHED DATA VALID
    # 400 = BAD REQUEST
    # 404 = CLIENT ERROR
    # 409 = AUTH DETAILS INVALID OR API CALLS EXCEEDED
    # 500 - INTERNAL SERVER ERROR

    def __init__(self, app_id, app_key):
        self.app_id = app_id
        self.app_key = app_key
        self.endpoint = "http://api.yummly.com/v1"
        self.s_rec_endpoint = self.endpoint + "/api/recipes"
        self.r_rec_endpoint = self.endpoint + "/api/recipe/"
        self.headers = {
            'X-Yummly-App-ID': self.app_id,
            'X-Yummly-App-Key': self.app_key
        }
        self.usr_rat = {}
        self.important_nutrients = [
            'ENERC_KCAL', 'CHOCDF', 'FAT', 'FASAT',
            'PROCNT', 'SUGAR', 'FIBTG', 'NA', 'CA', 'K',
            'FE', 'MG', 'ZN', 'VITA_IU', 'VITA_RAE',
            'VITB12', 'VITC', 'VITD-', 'VITK'
        ]

    def search_recipe(self, query=None, all_ing=None, excl_ing=None):

        if isinstance(query, str):
            payload = {'q': query}
        elif isinstance(query, list) and len(query) == 1:
            payload = {'q': query[0]}
        elif isinstance(query, list) and len(query) > 1:
            payload = {}
            if all_ing:
                all_ing.extend(query)
            else:
                all_ing = query
        else:
            payload = {}

        if all_ing:
            if isinstance(all_ing, list):
                payload['allowedIngredient'] = all_ing
            else:
                raise TypeError(msg='Allowed Ingredients only processed as dict')

        if excl_ing:
            if isinstance(excl_ing, list):
                payload['excludedIngredient'] = excl_ing
            else:
                raise TypeError(msg='Excluded Ingredients only processed as dict')

        # fix to skip recipes with empty flavor parameter
        payload['flavor.piquant.min'] = 0

        payload['maxResult'] = 400

        result = {}
        for i in xrange(5):
            payload['start'] = i * 400
            try:
                r = requests.get(self.s_rec_endpoint,
                                 params=payload,
                                 headers=self.headers
                                 )
                validate_status(r)
                if i == 0:
                    result = r.json()
                    del result['facetCounts']
                else:
                    tmp = r.json()['matches']
                    if tmp:
                        result['matches'].extend(tmp)
                    else:
                        break
            except ContentDecodingError:
                pass

        result['matches'] = self.refine_search_results(result['matches'])

        return result

    @staticmethod
    def refine_search_results(results):
        re_name = re_compile('^[A-Za-z &.,]*$')
        re_ingredient = re_compile('^[A-Za-z ]*$')

        return [result for result in results if
                re_name.match(result['recipeName']) and
                all(re_ingredient.match(i) for i in result['ingredients']) and result['flavors']]

    def recipe_details(self, uid):

        endpoint = self.r_rec_endpoint + uid + "?"
        r = requests.get(endpoint, headers=self.headers)
        validate_status(r)

        return r.json()

    def supply_ratings(self, rat):
        self.usr_rat = rat

    def rate_recipe_simple(self, recipe):

        total_diff = 0
        for usr, rec in zip(self.usr_rat.values(), recipe['flavors'].values()):
            total_diff += (1 - abs(usr - rec))

        return round((total_diff * 100) / 6, 1)

    @staticmethod
    def parse_info(r):
        print '*' * 30
        print ('{:*<30}'.format(' Matches presented: {0} ').format(len(r['matches'])))
        print ('{:*<30}'.format(' Total matches: {0} ').format(r['totalMatchCount']))
        for recipe in r['matches']:
            print '#' * 30
            print ' Recipe name: {0} '.format(recipe['recipeName'].encode('utf-8'))
            print ' Ingredients: {0} '.format(recipe['ingredients'])
            print ' ID: {0} '.format(recipe['id'].encode('utf-8'))
            print ' Cooking time: {0} '.format(recipe['totalTimeInSeconds'].encode('utf-8'))
            print ' Dishifier rating: {0} '.format(recipe['dishifier_rating'].encode('utf-8'))
