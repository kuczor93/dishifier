from __future__ import division

import random
from operator import itemgetter

from nltk import word_tokenize as word_tk, pos_tag
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from sqlalchemy import or_
from sqlalchemy import and_
from sklearn.neighbors import NearestNeighbors
import numpy as np


class NutrientDB:

    def __init__(self, db=None, usr=None, pwd=None):
        if usr:
            self.user = usr
        else:
            self.user = 'guest'
        if pwd:
            self.pwd = pwd
        else:
            self.pwd = 'dishifier'
        if db:
            self.db = db
        else:
            self.db = 'nutrient_db'

        self.compound_lookup = {
            'name': {},
            'flavour': {},
            'health-effects': {}
        }
        self.metadata = {
            'allergies': {},
            'food_compounds': {},
            'flavour_compounds': {},
            'diets': {}
        }
        self.wnl = WordNetLemmatizer()

        base = automap_base()

        # Creating MySQL ORM Engine to map object models to existing tables in nutrient database
        self.engine = create_engine('mysql://{0}:{1}@localhost/{2}'.format(self.user, self.pwd, self.db))

        # Reflecting the state of tables
        base.prepare(self.engine, reflect=True)

        # Allocating object models to respective class variables
        self.Compounds = base.classes.compounds
        self.Foods = base.classes.foods
        self.Flavors = base.classes.flavors
        self.Health_effects = base.classes.health_effects
        self.Compounds_foods = base.classes.compounds_foods
        self.Compounds_flavours = base.classes.compounds_flavors
        self.Compounds_health_effects = base.classes.compounds_health_effects

        # Instantiating session variable to allow database queries
        self.session = Session(self.engine)

    def analyze_input_ings(self, ings):
        # self.user_ing_cmpds = None
        for ing in ings:
            if ing not in self.metadata['food_compounds']:
                name, ing_cmpds = self.decompose(ing)
                self.metadata['food_compounds'][name] = ing_cmpds

    def analyze_recipe_ings(self, ings):
        cmpd_recipe_list = {}
        for ing in ings:
            if ing not in self.metadata['food_compounds']:
                name, compounds = self.decompose(ing)
                self.metadata['food_compounds'][name] = compounds

            if name in self.metadata['food_compounds']:
                cmpd_recipe_list[name] = self.metadata['food_compounds'][name]

        return cmpd_recipe_list

    def decompose(self, q):

        tokens = []     # all processed words for query
        res = []        # noun food ingredients
        ignore_list = [
            'teaspoon', 'tablespoon', 'fl', 'oz', 'cup', 'ml', 'inch', 'lb', 'kg', 'g', 'mg',
            'unavailable', 'warm', 'slice', 'piece', 'bite', 'strip', 'size', 'instant',
            'large', 'small', 'big', 'pound', 'chuck', 'cut', 'good', 'bad', 'one', 'section',
            'medium', 'a', 'dash', 'ounce', 'poultry', 'center', 'taste', 'bunch', 'beaten', 'sharp',
            'extra', 'package', 'mild', 'breakfast', 'sized', 'powder'
        ]
        if '(' in q:
            ing = []
            bracket_open = False
            for word in q.split():
                if '(' not in str(word) and ')' not in str(word):
                    if not bracket_open:
                        ing.append(word)
                else:
                    if '(' in word:
                        bracket_open = True
                    else:
                        bracket_open = False
            q = ' '.join(ing)

        # Stage 1: Tokenize ingredient
        tok_ing = pos_tag(word_tk(unicode(q).encode('ascii', 'ignore').lower()))
        tok_count = {}
        food_cat = None
        if len(tok_ing) > 1:
            for tok in tok_ing:
                if tok[1] in tok_count:
                    tok_count[tok[1]] += 1
                else:
                    tok_count[tok[1]] = 1

        for t in tok_ing:
            if t[1] in ['NNS', 'NNPS']:
                # Stage 2: Lemmatize each plural word
                lm = self.wnl.lemmatize(t[0])
            else:
                lm = t[0]
            if lm not in ignore_list:
                synsets = wn.synsets(lm)
                if any(s for s in synsets if 'NN' in t[1] and ('food' in s.lexname() or 'plant' in s.lexname())):
                    tokens.append(lm)
                    res.append(lm)
                    food_cat = self.find_food_id(t[0], lm)
                    if 'milk' in q:
                        if any(w in q for w in ['goat', 'buffalo', 'sheep']):
                            food_cat = {'s': t[0], 'p': lm, 'id': 712}
                        else:
                            food_cat = {'s': t[0], 'p': lm, 'id': 632}
                    elif 'oil' in q:
                        if 'palm' in q:
                            food_cat = {'s': t[0], 'p': lm, 'id': 605}
                        else:
                            food_cat = {'s': t[0], 'p': lm, 'id': 804}
                    elif 'bay' in q:
                        food_cat = {'s': 'bay', 'p': 'bay', 'id': 97}
                    elif ('pepper' in q and 'black' in q) or ('pepper' in tokens and len(tokens) == 1):
                        food_cat = {'s': t[0], 'p': lm, 'id': 139}
                else:
                    if not str(lm).isdigit() and synsets and t[1] not in ['CC', 'IN', 'RB', 'DT', 'VBD', 'VBN']:
                        tokens.append(lm)

        opts = ['{0}', '{0},%', '{0}%', '%{0}%']
        qr = None
        try:
            if res:
                if food_cat and isinstance(food_cat, dict):
                    for opt in opts:
                        for form in (food_cat['p'], food_cat['s']):
                            qr = self.session.query(self.Compounds_foods).filter(and_(
                                self.Compounds_foods.food_id == self.Foods.id, self.Foods.id == food_cat['id'],
                                self.Compounds_foods.orig_food_common_name.like(opt.format(form)))).all()
                            if qr:
                                break
                        else:
                            continue
                        if qr:
                            break
                    if len(qr) >= 1000:
                        qr = self.session.query(self.Compounds_foods).filter(and_(and_(
                            self.Compounds_foods.orig_food_common_name.like('%{0}%'.format(t)) for t in set(tokens)),
                            self.Compounds_foods.food_id == self.Foods.id, self.Foods.id == food_cat['id'])).all()
                    elif len(qr) <= 10:
                        qr = self.session.query(self.Compounds_foods).filter(and_(and_(
                            self.Compounds_foods.orig_food_common_name.like('%{0},%'.format(t)) for t in set(tokens)),
                            self.Compounds_foods.food_id == self.Foods.id, self.Foods.id == food_cat['id'])).all()
            if not qr:
                if food_cat and isinstance(food_cat, list):
                    qr = self.session.query(self.Compounds_foods).filter(and_(and_(
                        self.Compounds_foods.orig_food_common_name.like('%{0}%'.format(t)) for t in set(tokens)),
                        self.Compounds_foods.food_id == self.Foods.id),
                        or_(self.Foods.id == val for val in food_cat)).all()
                else:
                    for opt in opts:
                        qr = self.session.query(self.Compounds_foods).filter(and_(and_(
                                self.Compounds_foods.orig_food_common_name.like(opt.format(t)) for t in set(tokens)),
                                self.Compounds_foods.food_id == self.Foods.id)).all()
                        if not qr:
                            if res[-1]:
                                qr = self.session.query(self.Compounds_foods).filter(and_(
                                    self.Compounds_foods.orig_food_common_name.like(opt.format(res[-1])),
                                    self.Compounds_foods.food_id == self.Foods.id)).all()
                        if qr:
                            break
        except IndexError as err:
            print 'error: ', err
            print 'q: ', q
            print 'res: ', res
            print 'tokens: ', tokens

        c_id = set(cf.food_id for cf in qr)

        if len(c_id) == 1:
            vals = list(set([cf.compound_id for cf in qr]))
        else:
            vals = []

        return ' '.join(set(tokens)), vals

    def find_food_id(self, s, p):
        opts = ['{0}', '{0}%', '%{0}%']
        for opt in opts:
            qr = self.session.query(self.Foods).filter(or_(
                self.Foods.name.like(opt.format(s)), self.Foods.name.like(opt.format(p)))).all()
            if qr:
                if len(qr) == 1:
                    return {'id': qr[0].id, 's': s, 'p': p}
                else:
                    return [f.id for f in qr]
        else:
            return None

    def get_compound_data(self, cid):
        if isinstance(cid, set) or isinstance(cid, list):
            cmpd_list = {}
            if cid:
                for c in cid:
                    cmpd_name = self.session.query(self.Compounds).filter(self.Compounds.id == c).first().name
                    if c not in self.compound_lookup['name']:
                        self.compound_lookup['name'][c] = cmpd_name
                    cmpd_list[c] = self.compound_lookup['name'][c]
                return cmpd_list
            else:
                return []
        else:
            return self.session.query(self.Compounds).filter(self.Compounds.id == int(cid)).first().name

    def get_flavour_data(self, cid):
        if isinstance(cid, set) or isinstance(cid, list):
            cmpd_flavour_list = {}
            if cid:
                for c in cid:
                    flavors = self.session.query(self.Flavors).filter(and_(
                        self.Compounds_flavours.compound_id == c,
                        self.Compounds_flavours.flavor_id == self.Flavors.id)).all()
                    if flavors:
                        if c not in self.compound_lookup['flavour']:
                            self.compound_lookup['flavour'][c] = [f.name for f in flavors]
                        cmpd_flavour_list[c] = self.compound_lookup['flavour'][c]
                    else:
                        cmpd_flavour_list[c] = ['None']
                return cmpd_flavour_list
            else:
                return ['None']
        else:
            flavors = self.session.query(self.Flavors).filter(and_(
                self.Compounds_flavours.compound_id == cid,
                self.Compounds_flavours.flavor_id == self.Flavors.id)).all()
            if flavors:
                if cid not in self.compound_lookup['flavour']:
                    self.compound_lookup['flavour'][cid] = [f.name for f in flavors]
                return self.compound_lookup['flavour'][cid]
            else:
                return ['None']

    def get_health_effects_data(self, cid):
        if isinstance(cid, set) or isinstance(cid, list):
            cmpd_he_list = {}
            if cid:
                for c in cid:
                    he = self.session.query(self.Health_effects).filter(and_(
                        self.Compounds_health_effects.compound_id == c,
                        self.Compounds_health_effects.health_effect_id == self.Health_effects.id)).all()
                    if he:
                        if c not in self.compound_lookup['health-effects']:
                            self.compound_lookup['health-effects'][c] = [h.name for h in he]
                        cmpd_he_list[c] = self.compound_lookup['health-effects'][c]
                    else:
                        cmpd_he_list[c] = ['None']
                return cmpd_he_list
            else:
                return ['None']
        else:
            he = self.session.query(self.Health_effects).filter(and_(
                self.Compounds_health_effects.compound_id == cid,
                self.Compounds_health_effects.health_effect_id == self.Health_effects.id)).all()
            if he:
                if cid not in self.compound_lookup['health-effects']:
                    self.compound_lookup['health-effects'][cid] = [h.name for h in he]
                return self.compound_lookup['health-effects'][cid]
            else:
                return ['None']

    def pull_compound_flavors(self):

        for f in ['meaty', 'sour', 'spicy', 'sweet', 'bitter', 'savory']:
            q = self.session.query(self.Compounds_flavours, self.Flavors).filter(
                self.Compounds_flavours.flavor_id == self.Flavors.id, self.Flavors.name.like(f)).all()
            if f == 'savory':
                f_list = self.metadata['flavour_compounds'].setdefault('meaty', [])
            else:
                f_list = self.metadata['flavour_compounds'].setdefault(f, [])
            f_list.extend([str(cmpd[0].compound_id) for cmpd in q])

    def load_allergies(self):
        self.metadata['allergies']['Dairy-Free'] = '396^Dairy-Free'
        self.metadata['allergies']['Egg-Free'] = '397^Egg-Free'
        self.metadata['allergies']['Gluten-Free'] = '393^Gluten-Free'
        self.metadata['allergies']['Peanut-Free'] = '394^Peanut-Free'
        self.metadata['allergies']['Seafood-Free'] = '398^Seafood-Free'
        self.metadata['allergies']['Sesame-Free'] = '399^Sesame-Free'
        self.metadata['allergies']['Soy-Free'] = '400^Soy-Free'
        self.metadata['allergies']['Sulfite-Free'] = '401^Sulfite-Free'
        self.metadata['allergies']['Tree Nut-Free'] = '395^Tree Nut-Free'
        self.metadata['allergies']['Wheat-Free'] = '392^Wheat-Free'

    def load_diets(self):
        self.metadata['diets']['Lacto vegetarian'] = '388^Lacto vegetarian'
        self.metadata['diets']['Ovo vegetarian'] = '389^Ovo vegetarian'
        self.metadata['diets']['Vegan'] = '386^Vegan'
        self.metadata['diets']['Pescetarian'] = '390^Pescetarian'
        self.metadata['diets']['Vegetarian'] = '387^Lacto-ovo vegetarian'
        self.metadata['diets']['Paleo'] = '403^Paleo'


class RecipesDB:

    def __init__(self, db=None, usr=None, pwd=None):
        if usr:
            self.user = usr
        else:
            self.user = 'guest'
        if pwd:
            self.pwd = pwd
        else:
            self.pwd = 'dishifier'
        if db:
            self.db = db
        else:
            self.db = 'recipes_db'

        base = automap_base()

        # Creating MySQL ORM Engine to map object models to existing tables in recipes database
        self.engine = create_engine('mysql://{0}:{1}@localhost/{2}'.format(self.user, self.pwd, self.db))

        # Reflecting the state of tables
        base.prepare(self.engine, reflect=True)

        # Allocating object models to respective class variables
        self.Flavours = base.classes.Flavours
        self.Recipes = base.classes.Recipes
        self.Ingredients = base.classes.Ingredients
        self.Courses = base.classes.Courses
        self.Cuisines = base.classes.Cuisines
        self.Holidays = base.classes.Holidays
        self.Recipe_courses = base.classes.Recipe_courses
        self.Recipe_cuisines = base.classes.Recipe_cuisines
        self.Recipe_holidays = base.classes.Recipe_holidays
        self.Recipe_ingredients = base.classes.Recipe_ingredients

        # Instantiating session variable to allow database queries
        self.session = Session(self.engine)

    def insert_recipe_ingredient(self, r_id, i_id):
        tmp = self.Recipe_ingredients(recipe_id=r_id, ingredient_id=i_id)
        self.session.add(tmp)
        self.session.flush()

    def insert_recipe_course(self, r_id, c_id):
        tmp = self.Recipe_courses(recipe_id=r_id, course_id=c_id)
        self.session.add(tmp)
        self.session.flush()

    def insert_recipe_cuisine(self, r_id, c_id):
        tmp = self.Recipe_cuisines(recipe_id=r_id, cuisine_id=c_id)
        self.session.add(tmp)
        self.session.flush()

    def insert_recipe_holiday(self, r_id, h_id):
        tmp = self.Recipe_holidays(recipe_id=r_id, holiday_id=h_id)
        self.session.add(tmp)
        self.session.flush()

    def insert_flavour(self, flavour_dict):

        tmp = self.Flavours(**flavour_dict)
        self.session.add(tmp)
        self.session.flush()
        return tmp.id

    def insert_ingredient(self, ing):

        instance = self.session.query(self.Ingredients).filter(self.Ingredients.name == ing).first()
        if not instance:
            tmp = self.Ingredients(name=ing)
            self.session.add(tmp)
            self.session.flush()
            return tmp.id
        else:
            return instance.id

    def insert_course(self, course):

        instance = self.session.query(self.Courses).filter(self.Courses.name == course).first()
        if not instance:
            tmp = self.Courses(name=course)
            self.session.add(tmp)
            self.session.flush()
            return tmp.id
        else:
            return instance.id

    def insert_cuisine(self, cuisine):

        instance = self.session.query(self.Cuisines).filter(self.Cuisines.name == cuisine).first()
        if not instance:
            tmp = self.Cuisines(name=cuisine)
            self.session.add(tmp)
            self.session.flush()
            return tmp.id
        else:
            return instance.id

    def insert_holiday(self, holiday):

        instance = self.session.query(self.Holidays).filter(self.Holidays.name == holiday).first()
        if not instance:
            tmp = self.Holidays(name=holiday)
            self.session.add(tmp)
            self.session.flush()
            return tmp.id
        else:
            return instance.id

    def insert_recipe(self, recipe):

        instance = self.session.query(self.Recipes).filter(self.Recipes.yummly_id == recipe['id']).first()

        if not instance:

            flavour_id = self.insert_flavour(recipe['flavors'])

            recipe_vars = {
                'yummly_id': recipe['id'],
                'name': recipe['recipeName'],
                'flavour_id': flavour_id
            }

            if 'totalTimeInSeconds' in recipe:
                recipe_vars['total_time_in_seconds'] = recipe['totalTimeInSeconds']
            if 'smallImageUrls' in recipe:
                recipe_vars['img_preview'] = recipe['smallImageUrls'][0]

            tmp = self.Recipes(**recipe_vars)
            self.session.add(tmp)
            self.session.flush()

            if 'cuisine' in recipe['attributes']:
                for c in recipe['attributes']['cuisine']:
                    c_id = self.insert_cuisine(c)
                    self.insert_recipe_cuisine(tmp.id, c_id)
            if 'course' in recipe['attributes']:
                for c in recipe['attributes']['course']:
                    c_id = self.insert_course(c)
                    self.insert_recipe_course(tmp.id, c_id)
            if 'holiday' in recipe['attributes']:
                for h in recipe['attributes']['holiday']:
                    h_id = self.insert_holiday(h)
                    self.insert_recipe_holiday(tmp.id, h_id)

            for ing in recipe['ingredients']:
                i_id = self.insert_ingredient(ing.encode('utf-8'))
                self.insert_recipe_ingredient(tmp.id, i_id)

            self.session.commit()

    def pull_training_recipes(self, yummly_id_set):

        results = {
            'matches': [],
            'totalMatchCount': 45
        }
        # 45 results + (18) 2 backup stages in case results dont have picture
        ind = random.sample(xrange(len(yummly_id_set)), 63)
        for i in ind:
            r = self.get_recipe(yummly_id_set[i])
            if r['smallImageUrls']:
                results['matches'].append(r)
        results['matches'] = results['matches'][:45]

        return results

    def get_recipe(self, uid):

        recipe = self.session.query(self.Recipes).filter(self.Recipes.yummly_id == uid).first()
        if recipe.img_preview:
            imgs = [recipe.img_preview]
        else:
            imgs = []
        result = {
            'id': uid,
            'smallImageUrls': imgs,
            'recipeName': recipe.name,
            'totalTimeInSeconds': recipe.total_time_in_seconds,
            'flavors': self.session.query(self.Flavours).filter(
                     self.Flavours.id == recipe.flavour_id).first().__dict__,
            'attributes': {
                'course': [c.name for c in self.session.query(self.Courses).filter(
                    and_(self.Courses.id == self.Recipe_courses.course_id,
                         self.Recipe_courses.recipe_id == recipe.id))],
                'cuisine': [c.name for c in self.session.query(self.Cuisines).filter(
                    and_(self.Cuisines.id == self.Recipe_cuisines.cuisine_id,
                         self.Recipe_cuisines.recipe_id == recipe.id))],
                'holiday': [h.name for h in self.session.query(self.Holidays).filter(
                    and_(self.Holidays.id == self.Recipe_holidays.holiday_id,
                         self.Recipe_holidays.recipe_id == recipe.id))]
            },
            'ingredients': [i.name for i in self.session.query(self.Ingredients).filter(
                    and_(self.Ingredients.id == self.Recipe_ingredients.ingredient_id,
                         self.Recipe_ingredients.recipe_id == recipe.id))]
        }

        # removing unnecessary keys from 'flavors' dict
        del result['flavors']['_sa_instance_state']
        del result['flavors']['id']

        return result

    def query_recipes(self, ingredients):

        results = {
            'matches': [],
            'totalMatchCount': 0
        }

        recipes_info = self.session.query(self.Recipes, self.Recipe_ingredients).filter(
            and_(
                or_(self.Ingredients.name == i for i in ingredients),
                self.Ingredients.id == self.Recipe_ingredients.ingredient_id,
                self.Recipes.id == self.Recipe_ingredients.recipe_id
            )).all()

        results['totalMatchCount'] = len(recipes_info)
        for recipe, _ in recipes_info:
            r = {
                'id': recipe.yummly_id,
                'smallImageUrls': [recipe.img_preview],
                'recipeName': recipe.name,
                'totalTimeInSeconds': recipe.total_time_in_seconds,
                'flavors': self.session.query(self.Flavours).filter(
                    self.Flavours.id == recipe.flavour_id).first().__dict__,
                'attributes': {
                    'course': [c.name for c in self.session.query(self.Courses).filter(
                        and_(self.Courses.id == self.Recipe_courses.course_id,
                             self.Recipe_courses.recipe_id == recipe.id))],
                    'cuisine': [c.name for c in self.session.query(self.Cuisines).filter(
                        and_(self.Cuisines.id == self.Recipe_cuisines.cuisine_id,
                             self.Recipe_cuisines.recipe_id == recipe.id))],
                    'holiday': [h.name for h in self.session.query(self.Holidays).filter(
                        and_(self.Holidays.id == self.Recipe_holidays.holiday_id,
                             self.Recipe_holidays.recipe_id == recipe.id))]
                },
                'ingredients': [i.name for i in self.session.query(self.Ingredients).filter(
                    and_(self.Ingredients.id == self.Recipe_ingredients.ingredient_id,
                         self.Recipe_ingredients.recipe_id == recipe.id))]
            }

            # removing unnecessary keys from 'flavors' dict
            del r['flavors']['_sa_instance_state']
            del r['flavors']['id']

            results['matches'].append(r)

        return results

    def database_lookup(self, mode='i'):
        if mode == 'i':
            return [i.name for i in self.session.query(self.Ingredients).all()]
        elif mode == 'r':
            return {i.name: i.yummly_id for i in self.session.query(self.Recipes).all()}

    def make_recommendations(self, p):

        cuisines = [c.name for c in self.session.query(self.Cuisines).all()]
        courses = [c.name for c in self.session.query(self.Courses).all()]
        train_data = []
        user_data = []
        indices = []

        for r in self.session.query(self.Recipes).all():
            r_data = []
            indices.append(str(r.yummly_id))
            rcu = self.session.query(self.Recipe_cuisines).filter(self.Recipe_cuisines.recipe_id == r.id).all()
            for c in cuisines:
                if c in rcu:
                    r_data.append(1.)
                else:
                    r_data.append(0.)
            rco = self.session.query(self.Recipe_courses).filter(self.Recipe_courses.recipe_id == r.id).all()
            for c in courses:
                if c in rco:
                    r_data.append(1.)
                else:
                    r_data.append(0.)
            f_attr = self.session.query(self.Flavours).filter(self.Flavours.id == r.flavour_id).first().__dict__
            for val in f_attr.itervalues():
                if isinstance(val, float):
                    r_data.append(val)

            train_data.append(r_data)
            if r.yummly_id in p.favourite['recipes']:
                user_data.append(r_data)

        #########################################
        user_pref = []
        for c in cuisines:
            if c in p.preferences['cuisines']:
                user_pref.append(1.)
            else:
                user_pref.append(0.)
        for c in courses:
            if c in p.preferences['courses']:
                user_pref.append(1.)
            else:
                user_pref.append(0.)
        for val in p.preferences['flavour_attrs'].values():
            user_pref.append(val)
        user_data.append(user_pref)
        #########################################

        knn = NearestNeighbors(n_neighbors=51).fit(train_data)
        dist, ind = knn.kneighbors(user_data)
        tmp = {i: a for a, i in zip(np.concatenate(dist), np.concatenate(ind))}
        uniq, index = np.unique([val[0] for val in sorted(tmp.items(), key=itemgetter(1))], return_index=True)
        rec = [indices[i] for i in uniq[index.argsort()]]
        return dist, rec

    def clear(self):
        self.session.query(self.Recipe_ingredients).filter().delete(synchronize_session=False)
        self.session.query(self.Recipe_courses).filter().delete(synchronize_session=False)
        self.session.query(self.Recipe_cuisines).filter().delete(synchronize_session=False)
        self.session.query(self.Recipe_holidays).filter().delete(synchronize_session=False)
        self.session.query(self.Ingredients).filter().delete(synchronize_session=False)
        self.session.query(self.Cuisines).filter().delete(synchronize_session=False)
        self.session.query(self.Courses).filter().delete(synchronize_session=False)
        self.session.query(self.Holidays).filter().delete(synchronize_session=False)
        self.session.query(self.Recipes).filter().delete(synchronize_session=False)
        self.session.query(self.Flavours).filter().delete(synchronize_session=False)
        self.session.commit()
