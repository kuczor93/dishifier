from __future__ import division
from requests import codes as r_codes
from os import system
from platform import system as detect_os
from urllib2 import urlopen, URLError

"""
    Utils is an auxiliary class that supplies the rest of the application
    with important, common functions to extend class functionality
    as well as provide some code redundancy.
"""


def get_input(msg=None, **kwargs):
    try:
        if msg:
            val = raw_input(msg)
        else:
            val = raw_input()
    except NameError:
        if msg:
            val = input(msg)
            return val
        else:
            val = input()
            return val
    if kwargs:
        if kwargs['digit']:
            try:
                val = int(val)
            except ValueError:
                pass
        elif kwargs['decimal']:
            try:
                val = float(val)
            except ValueError:
                pass

    return val


def convert_weight(attr, value, weight):
        weight_table = {
            'VITA_IU': 0.6,
            'VITA_RAE': 0,
            'VITD-': 0.025
        }   # mcg
        mtpl = weight_table.setdefault(attr, 1)
        if mtpl == 1:
            if value < 10:
                return round(value, 1), weight
            else:
                return int(value), weight
        else:
            if value*mtpl < 10:
                return round(value*mtpl, 1), 'mcg'
            else:
                return int(value*mtpl), 'mcg'


def get_rda(t, attr, value):
    nutrient_table = {
        'ENERC_KCAL': {'male': 2500, 'female': 2000},
        'CHOCDF': {'male': 300, 'female': 300},
        'SUGAR': {'male': 120, 'female': 90},
        'FAT': {'male': 625, 'female': 500},
        'FASAT': {'male': 20, 'female': 20},
        'FIBTG': {'male': 24, 'female': 24},
        'PROCNT': {'male': 55, 'female': 45},
        'VITB12': {'male': 1, 'female': 1},
        'VITC': {'male': 60, 'female': 60},
        'VITD-': {'male': 600, 'female': 600},
        'VITA_IU': {'male': 900, 'female': 900},
        'VITA_RAE': {'male': 900, 'female': 900},
        'VITK': {'male': 120, 'female': 120},
        'CA': {'male': 800, 'female': 800},
        'MG': {'male': 300, 'female': 300},
        'NA': {'male': 1500, 'female': 1500},
        'K': {'male': 4700, 'female': 4700},
        'FE': {'male': 14, 'female': 14},
        'ZN': {'male': 15, 'female': 15}
    }
    r = (value / nutrient_table[attr][t]) * 100
    if (r - round(r)) == 0:
        return 'Nil'
    else:
        if r < 1:
            return '< 1%'
        else:
            return '{0}%'.format(round(r, 1))


def validate_status(response):

    if response.status_code == r_codes.ok:
        pass
    elif response.status_code == r_codes.bad_request:
        raise ValueError("Invalid parameters in following url:\n{0}".format(response.url))
    elif response.status_code == r_codes.conflict:
        raise StandardError("API calls limit exceeded or invalid API key/id")
    elif response.status_code == r_codes.internal_server_error:
        raise StandardError("Internal Server Error: {0}".format(response.reason))
    else:
        raise StandardError(response.reason)


def clear_terminal():
    if detect_os() == 'Linux':
        system('clear')
    else:
        system('cls')


def check_connection():
    try:
        urlopen('http://74.125.198.94', timeout=1)
        return True
    except URLError:
        return False
