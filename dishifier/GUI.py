from PyQt4 import QtCore, QtGui
from os.path import dirname, join
from Databases import RecipesDB, NutrientDB
from operator import itemgetter
from urllib2 import urlopen
from Yummly import Yummly
from re import split as re_split
from PyQt4.QtCore import QThread
import sip
import User
import Utils

ICONS_DIR = join(dirname(dirname(__file__)), 'icons')
CURRENT_PROFILE = None


def resize(obj, w, h):
    obj.resize(w, h)
    obj.setMaximumSize(w, h)
    obj.setMinimumSize(w, h)


def set_profile(p):
    global CURRENT_PROFILE
    CURRENT_PROFILE = p


def center(obj):
    qr = obj.frameGeometry()
    cp = QtGui.QDesktopWidget().availableGeometry().center()
    qr.moveCenter(cp)
    obj.move(qr.topLeft())


def split_str(string, div=4):
    tmp = string.split()
    if len(tmp) > div:
        retval = []
        for i, w in enumerate(tmp):
            if i != 0:
                retval.append('\n')
            retval.extend(tmp[i * div:(i + 1) * div])
        return " ".join(retval)
    else:
        return string


class LoginForm(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.login_box = QtGui.QLineEdit(self)
        self.login_button = QtGui.QPushButton('Login')

        self.setup_ui()

    def setup_ui(self):

        self.setWindowTitle('Dishifier Login Form')
        self.setAutoFillBackground(True)
        self.setWindowIcon(QtGui.QIcon(QtGui.QPixmap(join(ICONS_DIR, 'icon.png'))))
        resize(self, 500, 250)
        center(self)

        hl = QtGui.QHBoxLayout()
        vl = QtGui.QVBoxLayout()
        hl.setSpacing(15)
        vl.setSpacing(25)

        p = self.palette()
        p.setColor(self.backgroundRole(), QtCore.Qt.lightGray)
        self.setPalette(p)

        logo = QtGui.QLabel(self)
        logo.setPixmap(QtGui.QPixmap(join(ICONS_DIR, 'login_logo.png')))

        v_line = QtGui.QFrame()
        v_line.setFrameStyle(QtGui.QFrame.VLine)
        v_line.setFrameShadow(QtGui.QFrame.Sunken)

        self.login_box.setMaximumSize(120, 35)
        self.login_box.setAlignment(QtCore.Qt.AlignHCenter)
        self.login_button.setMaximumSize(120, 35)

        vl.addWidget(self.login_box)
        vl.addWidget(self.login_button)
        vl.setAlignment(QtCore.Qt.AlignHCenter)

        hl.addWidget(logo)
        hl.addWidget(v_line)
        hl.addLayout(vl)
        self.setLayout(hl)

        self.login_button.setFocus()
        self.show()

    def closeEvent(self, event):
        msg = MessageBox(self, 'Are you sure to quit?', QtGui.QMessageBox.Question)
        reply = msg.exec_()
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


class MainWindow(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)

        self.tr_data = None
        self.cleanlooks_style = QtGui.QStyleFactory().create('cleanlooks')
        self.transition_btn = QtGui.QPushButton()
        self.update_btn = QtGui.QPushButton()
        self.clear_btn = QtGui.QPushButton()
        self.add_btn = QtGui.QPushButton()
        self.search_box = QtGui.QLineEdit()
        self.universal_msg_box = MessageBox()
        self.universal_label = QtGui.QLabel()
        self.universal_model = QtGui.QStandardItemModel()
        self.universal_layout = QtGui.QVBoxLayout()
        self.details_widget = QtGui.QWidget()
        self.compound_widget = QtGui.QWidget()
        self.loading_box = ProcessingBox(self)
        self.search_results = {'matches': []}
        self.worker = None
        self.connected = True   # defines whether network connection is active

        self.ing_icon = QtGui.QPushButton(self)
        self.recipe_icon = QtGui.QPushButton(self)
        self.nut_icon = QtGui.QPushButton(self)
        self.recommend_icon = QtGui.QPushButton(self)
        self.account_icon = QtGui.QPushButton(self)
        self.training_icon = QtGui.QPushButton(self)
        self.logout_icon = QtGui.QPushButton(self)

        self.yummly = Yummly('f7b9e993', 'cfa23e905cd6ed734a1fb33514e455b1')
        self.nut_db = NutrientDB()
        self.rec_db = RecipesDB()

        self.work_area = QtGui.QFrame()
        self.hl = QtGui.QHBoxLayout()

        self.setup_ui()
        self.ings = self.rec_db.database_lookup()
        self.recipes = self.rec_db.database_lookup(mode='r')
        self.nut_db.pull_compound_flavors()
        self.nut_db.load_diets()
        self.nut_db.load_allergies()
        self.tr_buttons = []

    def setup_ui(self):

        self.setWindowTitle('Dishifier')
        self.setWindowIcon(QtGui.QIcon(QtGui.QPixmap(join(ICONS_DIR, 'icon.png'))))
        self.setWindowFlags(QtCore.Qt.WindowTitleHint)
        resize(self, 1024, 800)
        center(self)

        vl_menu = QtGui.QVBoxLayout()

        self.ing_icon.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.recipe_icon.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.nut_icon.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.recommend_icon.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.training_icon.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.account_icon.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.logout_icon.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)

        self.load_icons()

        v_line = QtGui.QFrame()
        v_line.setFrameStyle(QtGui.QFrame.VLine)
        v_line.setFrameShadow(QtGui.QFrame.Sunken)

        vl_menu.addWidget(self.ing_icon)
        vl_menu.addWidget(self.recipe_icon)
        vl_menu.addWidget(self.nut_icon)
        vl_menu.addWidget(self.recommend_icon)
        vl_menu.addWidget(self.training_icon)
        vl_menu.addWidget(self.account_icon)
        vl_menu.addWidget(self.logout_icon)

        QtCore.QObject.connect(self.ing_icon, QtCore.SIGNAL('clicked()'), self.recipes_by_ingredients)
        QtCore.QObject.connect(self.recipe_icon, QtCore.SIGNAL('clicked()'), self.recipe_search)
        QtCore.QObject.connect(self.nut_icon, QtCore.SIGNAL('clicked()'), self.ingredient_compound_details)
        QtCore.QObject.connect(self.recommend_icon, QtCore.SIGNAL('clicked()'), self.recommendations)
        QtCore.QObject.connect(self.training_icon, QtCore.SIGNAL('clicked()'), self.training_system)
        QtCore.QObject.connect(self.account_icon, QtCore.SIGNAL('clicked()'), self.account_overview)
        QtCore.QObject.connect(self.logout_icon, QtCore.SIGNAL('clicked()'), self.save_data)

        self.hl.addLayout(vl_menu)
        self.hl.addWidget(v_line)

        self.setLayout(self.hl)

    def load_icons(self):
        for btn, icon in zip([self.ing_icon, self.recipe_icon, self.nut_icon, self.recommend_icon,
                              self.account_icon, self.training_icon, self.logout_icon],
                             ['recipe_by_ingredients.png', 'local_recipe_search.png', 'decomposer.png',
                              'recommendations.png', 'account.png', 'flavour_training.png', 'logout.png']):
            qp = QtGui.QPixmap(join(ICONS_DIR, icon))
            btn.setFixedSize(QtCore.QSize(100, 100))
            btn.setIcon(QtGui.QIcon(qp))
            btn.setIconSize(QtCore.QSize(95, 95))
            # btn.setContentsMargins

    def add_result_item(self, result):
        self.connected = Utils.check_connection()
        qf = QtGui.QFrame()
        qf.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        qf.setAutoFillBackground(True)
        qf.setFrameStyle(QtGui.QFrame.Panel)
        qf.setMaximumHeight(150)

        result_layout = QtGui.QHBoxLayout(qf)
        desc_layout = QtGui.QVBoxLayout()
        df_layout = QtGui.QVBoxLayout()

        lbl = ImageLabel(result['id'])
        lbl.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        lbl.setMinimumWidth(172)  # This is due to picture preview being 150 px wide + 22 px of margin
        if self.connected:
            if 'smallImageUrls' in result:
                img = urlopen(str(result['smallImageUrls'][-1][:-4])).read()
                qp = QtGui.QPixmap()
                qp.loadFromData(img)
            else:
                qp = QtGui.QPixmap(join(ICONS_DIR, 'no_image.png'))
        else:
                qp = QtGui.QPixmap(join(ICONS_DIR, 'no_image.png'))

        QtCore.QObject.connect(lbl, QtCore.SIGNAL('clicked()'), self.recipe_details)
        lbl.setPixmap(qp.scaledToHeight(120))
        lbl.setAlignment(QtCore.Qt.AlignHCenter)
        result_layout.addWidget(lbl)

        name = QtGui.QLabel(str(result['recipeName']))
        desc_layout.addStretch(1)
        name.setWordWrap(True)
        name.setMaximumWidth(400)
        name.setFont(QtGui.QFont('Arial', 13, QtGui.QFont.Bold))
        desc_layout.addWidget(name)
        desc_layout.addStretch(1)
        time = QtGui.QLabel('Total preparation time: <b>{0}</b>'.format(str(result['totalTimeInSeconds'])))
        time.setFont(QtGui.QFont('Arial', 12))
        desc_layout.addWidget(time)
        desc_layout.addStretch(1)
        desc_layout.setContentsMargins(15, 0, 15, 0)
        result_layout.addLayout(desc_layout)
        result_layout.addStretch(1)

        rating = self.rate_by_color(str(result['df_rating']))
        df_layout.setContentsMargins(15, 0, 25, 0)
        df_layout.addWidget(rating)
        result_layout.addLayout(df_layout)

        qf.setFrameShadow(QtGui.QFrame.Sunken)
        return qf

    def rate_by_color(self, rating):
        r = float(rating)
        ql = QtGui.QLabel(rating)
        p = self.palette()
        if r < 30:
            p.setColor(ql.foregroundRole(), QtGui.QColor(255, 0, 0))
        elif r < 60:
            p.setColor(ql.foregroundRole(), QtGui.QColor(255, 128, 0))
        elif r < 80:
            p.setColor(ql.foregroundRole(), QtGui.QColor(255, 255, 0))
        elif r < 90:
            p.setColor(ql.foregroundRole(), QtGui.QColor(128, 255, 0))
        else:
            p.setColor(ql.foregroundRole(), QtGui.QColor(0, 204, 0))

        ql.setPalette(p)
        ql.setFont(QtGui.QFont('Arial', 24, QtGui.QFont.Bold))
        return ql

    def refresh(self):
        self.hl.removeWidget(self.work_area)
        sip.delete(self.work_area)
        self.work_area = QtGui.QFrame()

    @staticmethod
    def save_data():
        User.dump_profile(CURRENT_PROFILE)

    def add_ingredient(self):
        if self.search_box.text() and not str(self.search_box.text()).isspace():
            item = QtGui.QStandardItem(self.search_box.text())
            item.setCheckable(True)
            item.setCheckState(QtCore.Qt.Checked)
            self.universal_model.appendRow(item)
            self.search_box.clear()

    def find_rated_recipes(self, ings):
        if ings:
            if Utils.check_connection():
                self.search_results = self.yummly.search_recipe(all_ing=ings)
                insert_recipes = True
            else:
                self.search_results = self.rec_db.query_recipes(ings)
                insert_recipes = False
            self.yummly.supply_ratings(CURRENT_PROFILE.preferences['flavour_attrs'])
            if self.search_results['matches']:
                for r in self.search_results['matches']:
                    if insert_recipes:
                        self.rec_db.insert_recipe(r)
                    r['df_rating'] = self.yummly.rate_recipe_simple(r)

                sort_vals = {uid: rec['df_rating'] for uid, rec in enumerate(self.search_results['matches'])}
                self.search_results['rated_keys'] = sorted(sort_vals.items(), key=itemgetter(1), reverse=True)
                self.search_results['display_start'] = 10
                self.universal_label.setText('<b>Recipe count:</b> {0}'.format(len(self.search_results['matches'])))
                self.search_results['result_from_search'] = True

                return self.search_results['rated_keys'][:10]

    def add_rated_recipes(self, r_list):
        self.connected = Utils.check_connection()
        if not self.connected:
            self.universal_msg_box = MessageBox(self, 'Recipe images will not load due to no\n'
                                                      'active network connection. Please try again later!')
            self.universal_msg_box.exec_()
        for top_val in r_list:
            self.universal_layout.addWidget(self.add_result_item(self.search_results['matches'][top_val[0]]))

        if self.search_results['result_from_search']:
            target_layout = self.transition_btn.parentWidget().layout().itemAt(0)

            self.transition_btn.deleteLater()
            self.clear_btn = QtGui.QPushButton('Clear')
            self.clear_btn.setStyleSheet('background-color: rgb(0, 160, 210); color: white; border-radius: '
                                         '5px; border-style: raised; padding: 5px 15px 5px 15px')
            self.clear_btn.setStyle(self.cleanlooks_style)
            self.clear_btn.setMaximumWidth(100)
            target_layout.addWidget(self.clear_btn)

            QtCore.QObject.connect(self.clear_btn, QtCore.SIGNAL('clicked()'), self.recipes_by_ingredients)
            self.loading_box.finish()

    def start_searches(self):
        ings = [str(self.universal_model.item(i).text())
                for i in xrange(self.universal_model.rowCount())
                if self.universal_model.item(i).checkState() == QtCore.Qt.Checked]
        if ings:
            self.worker = BackgroundThread(self.find_rated_recipes, ings)
            self.worker.start()
            QtCore.QObject.connect(self.worker, QtCore.SIGNAL('finished(PyQt_PyObject)'), self.add_rated_recipes)
            self.loading_box.exec_()

    def stop_loading(self):
        self.loading_box.finish(True)

    def load_more_results(self):
        self.universal_msg_box = MessageBox(self)

        if self.search_results and 'rated_keys' in self.search_results:
            start_val = self.search_results['display_start']
            keys = self.search_results['rated_keys'][start_val:start_val + 10]
            if keys:
                self.search_results['result_from_search'] = False
                self.add_rated_recipes(keys)
                self.search_results['display_start'] += 10
            else:
                self.universal_msg_box.setText('No more results to display!')
                self.universal_msg_box.exec_()
                self.sender().setDisabled(True)
        else:
            self.universal_msg_box.setText('No results to display.\nPlease search first!')
            self.universal_msg_box.exec_()

    def recipes_by_ingredients(self):
        self.loading_box.finish(False)
        self.refresh()

        work_layout = QtGui.QHBoxLayout()
        area_splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        ingredient_area = QtGui.QListView()
        ingredient_area.setMaximumWidth(150)
        middle_frame = QtGui.QFrame()
        middle_layout = QtGui.QVBoxLayout()
        search_layout = QtGui.QHBoxLayout()
        recipe_scroll = QtGui.QScrollArea()
        recipe_widget = QtGui.QWidget()
        self.universal_layout = QtGui.QVBoxLayout(recipe_widget)
        self.update_btn = QtGui.QPushButton('Load more results')
        self.update_btn.setFixedSize(QtCore.QSize(150, 40))
        self.universal_label = QtGui.QLabel('<b>Recipe count:</b> -')
        self.universal_label.setStyleSheet("""font-size: 14px; color: rgb(120, 120, 120); font-family: Calibri;""")
        more_res_layout = QtGui.QHBoxLayout()
        more_res_layout.setContentsMargins(0, 10, 0, 0)
        more_res_layout.addWidget(self.universal_label)
        more_res_layout.addStretch()
        more_res_layout.addWidget(self.update_btn)
        more_res_layout.addStretch()
        more_res_layout.addStretch()

        search_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.transition_btn = QtGui.QPushButton('Search')
        self.transition_btn.setStyle(self.cleanlooks_style)
        self.transition_btn.setStyleSheet('background-color: rgb(230, 115, 0); color: white; border-radius: 5px; '
                                          'border-style: raised; padding: 5px 15px 5px 15px')
        self.transition_btn.setMaximumWidth(100)
        self.add_btn = QtGui.QPushButton('Add')
        self.add_btn.setMaximumWidth(100)

        self.search_box = QtGui.QLineEdit()
        self.search_box.setMaximumWidth(250)
        self.search_box.setMinimumWidth(250)
        completer = QtGui.QCompleter()
        self.search_box.setCompleter(completer)
        search_model = QtGui.QStringListModel()
        completer.setModel(search_model)
        search_model.setStringList(self.ings)

        search_layout.addWidget(self.search_box)
        search_layout.addWidget(self.add_btn)
        search_layout.addWidget(self.transition_btn)

        self.universal_model = QtGui.QStandardItemModel(ingredient_area)
        ingredient_area.setModel(self.universal_model)

        recipe_scroll.setWidget(recipe_widget)
        recipe_scroll.setWidgetResizable(True)
        middle_layout.addLayout(search_layout)
        middle_layout.addWidget(recipe_scroll)
        middle_layout.addLayout(more_res_layout)
        middle_frame.setLayout(middle_layout)
        area_splitter.addWidget(middle_frame)
        area_splitter.addWidget(ingredient_area)

        QtCore.QObject.connect(self.add_btn, QtCore.SIGNAL('clicked()'), self.add_ingredient)
        QtCore.QObject.connect(self.transition_btn, QtCore.SIGNAL('clicked()'), self.start_searches)
        QtCore.QObject.connect(self.update_btn, QtCore.SIGNAL('clicked()'), self.load_more_results)

        work_layout.addWidget(area_splitter)
        self.work_area.setLayout(work_layout)
        self.hl.addWidget(self.work_area, 1)

    def recommendations(self):
        self.connected = Utils.check_connection()
        if CURRENT_PROFILE.training['tutorial-complete']:
            if not self.connected:
                self.universal_msg_box = MessageBox(self, 'Cannot make recommendations!\n'
                                                          'No active network connection.')
                self.universal_msg_box.exec_()
                return
            if CURRENT_PROFILE.preferences['modified'] or CURRENT_PROFILE.favourite['modified'] \
                    or not CURRENT_PROFILE.recommendations:
                CURRENT_PROFILE.preferences['modified'] = False
                CURRENT_PROFILE.favourite['modified'] = False
                self.start_worker()
                self.loading_box.exec_()
                return
            suggestions = CURRENT_PROFILE.recommendations
            self.refresh()
            work_layout = QtGui.QVBoxLayout()
            scroll_area = QtGui.QScrollArea()
            scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
            scroll_area.setWidgetResizable(True)
            scroll_widget = QtGui.QWidget()
            scroll_layout = QtGui.QVBoxLayout(scroll_widget)
            scroll_layout.setMargin(0)
            scroll_layout.setSpacing(0)

            row_frame = None
            row_layout = None
            for i, r_id in enumerate(suggestions):
                recipe = self.rec_db.get_recipe(r_id)
                if i % 3 == 0:
                    if i != 0:
                        scroll_layout.addWidget(row_frame)
                    row_frame = QtGui.QFrame()
                    row_frame.setContentsMargins(0, 0, 0, 0)
                    row_frame.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
                    row_layout = QtGui.QHBoxLayout(row_frame)
                    row_layout.setMargin(0)
                    row_layout.setSpacing(0)

                lbl = ImageLabel(uid=r_id)
                if self.connected:
                    if 'smallImageUrls' in recipe and recipe['smallImageUrls']:
                        img = urlopen(str(recipe['smallImageUrls'][-1][:-4])).read()
                        qp = QtGui.QPixmap()
                        qp.loadFromData(img)
                    else:
                        qp = QtGui.QPixmap(join(ICONS_DIR, 'no_image.png'))
                else:
                    qp = QtGui.QPixmap(join(ICONS_DIR, 'no_image.png'))
                QtCore.QObject.connect(lbl, QtCore.SIGNAL('clicked()'), self.recipe_details)
                lbl.setPixmap(qp.scaledToWidth(272))
                lbl.setMargin(5)
                row_layout.addWidget(lbl)

            scroll_area.setWidget(scroll_widget)
            work_layout.addWidget(scroll_area)
        else:
            self.refresh()
            work_layout = QtGui.QVBoxLayout()
            lbl = QtGui.QLabel('To see your recommendations,\n'
                               'complete Flavour Training Program first !')
            lbl.setStyleSheet("""color: rgb(100, 100, 100); font-size: 20pt; font-family: Calibri;""")
            lbl.setAlignment(QtCore.Qt.AlignHCenter)
            logo = QtGui.QLabel()
            logo.setPixmap(QtGui.QPixmap(join(ICONS_DIR, 'login_logo.png')))
            logo.setAlignment(QtCore.Qt.AlignHCenter)
            work_layout.addWidget(lbl)
            work_layout.addWidget(logo)
            work_layout.setSpacing(50)
            work_layout.setAlignment(QtCore.Qt.AlignCenter)

        self.work_area.setLayout(work_layout)
        self.hl.addWidget(self.work_area, 1)

    def next_tr_stage(self):
        if CURRENT_PROFILE.training['stage'] == 6:
            for btn in self.tr_buttons[0]:
                if btn.selected:
                    CURRENT_PROFILE.load_preferences(btn.data)
            for btn in self.tr_buttons[1]:
                if btn.selected:
                    CURRENT_PROFILE.load_preferences(btn.data)
        else:
            for btn in self.tr_buttons:
                if btn.selected:
                    CURRENT_PROFILE.load_preferences(btn.data)

        if not CURRENT_PROFILE.training['tutorial-complete']:
            stage_limit = 6
        else:
            stage_limit = 5

        if CURRENT_PROFILE.training['stage'] < stage_limit:
            CURRENT_PROFILE.training['stage'] += 1
        else:
            CURRENT_PROFILE.reset_training_data()
            CURRENT_PROFILE.training['complete'] = True
        self.training_system()

    def training_system(self):
        self.connected = Utils.check_connection()
        if not CURRENT_PROFILE.training['tutorial-complete'] and CURRENT_PROFILE.training['stage'] == 0:
            self.refresh()
            work_layout = QtGui.QVBoxLayout()
            title = QtGui.QLabel('Flavour Training Program')
            title.setStyleSheet("""color: rgb(100, 100, 100); font-family: Calibri; font-size: 32pt;""")
            title.setAlignment(QtCore.Qt.AlignHCenter)
            logo = QtGui.QLabel()
            logo.setPixmap(QtGui.QPixmap(join(ICONS_DIR, 'login_logo.png')))
            logo.setAlignment(QtCore.Qt.AlignHCenter)
            instructions_layout = QtGui.QVBoxLayout()
            instructions_layout.setAlignment(QtCore.Qt.AlignHCenter)
            instructions_layout.setSpacing(10)
            for l in ['<u style="font-size: 20px;">Instructions:</u>', '- Each program consists of 5 stages.',
                      '- Initial program set provides interface to specify allergies and diets.',
                      '- Stage selection should reflect food choices that personally looks most appealing or\n'
                      'best suits daily eating style.',
                      '- Repeat program as many times as you like to improve recommendation matching.\n'
                      'The more you do, the better :)',
                      '- Please also visit "Account Overview" section to update your profile']:
                ql = QtGui.QLabel(l)
                ql.setStyleSheet("""color: rgb(100, 100, 100); font-size: 16px; font-family: Calibri;""")
                ql.setAlignment(QtCore.Qt.AlignHCenter)
                instructions_layout.addWidget(ql)

            self.transition_btn = QtGui.QPushButton('Start training!')
            self.transition_btn.setStyle(self.cleanlooks_style)
            self.transition_btn.setFixedSize(QtCore.QSize(150, 40))
            btn_layout = QtGui.QHBoxLayout()
            btn_layout.setSpacing(0)
            btn_layout.setMargin(0)
            btn_layout.addStretch()
            btn_layout.addWidget(self.transition_btn)
            btn_layout.addStretch()
            work_layout.addWidget(title)
            work_layout.addWidget(logo)
            work_layout.addLayout(instructions_layout)
            work_layout.addLayout(btn_layout)
            work_layout.setSpacing(30)
            work_layout.setAlignment(QtCore.Qt.AlignCenter)
            self.work_area.setLayout(work_layout)
            self.hl.addWidget(self.work_area, 1)

            QtCore.QObject.connect(self.transition_btn, QtCore.SIGNAL('clicked()'), self.next_tr_stage)
        else:
            if CURRENT_PROFILE.training['stage'] == 6:
                self.refresh()
                self.tr_buttons = [[], []]
                work_layout = QtGui.QVBoxLayout()
                work_layout.setSpacing(25)
                #################################################
                allergy_frame = QtGui.QFrame()
                allergy_layout = QtGui.QVBoxLayout(allergy_frame)
                allergy_layout.setSpacing(5)
                allergy_lbl = QtGui.QLabel('Please select all appropriate food allergies, if any')
                allergy_lbl.setMaximumHeight(100)
                allergy_lbl.setAlignment(QtCore.Qt.AlignCenter)
                allergy_lbl.setStyleSheet("""font-size: 16pt;""")
                allergy_grid = QtGui.QGridLayout()
                positions = [(i, j) for i in xrange(5) for j in xrange(2)]
                for allergy, pos in zip(self.nut_db.metadata['allergies'].iterkeys(), positions):
                    btn = SelectionButton(data={'allergy': allergy}, text=allergy)
                    btn.setStyle(self.cleanlooks_style)
                    self.tr_buttons[0].append(btn)
                    allergy_grid.addWidget(btn, *pos)
                allergy_layout.addWidget(allergy_lbl)
                allergy_layout.addLayout(allergy_grid)
                #################################################
                h_line = QtGui.QFrame()
                h_line.setFrameShape(QtGui.QFrame.HLine)
                h_line.setFrameShadow(QtGui.QFrame.Sunken)
                #################################################
                diet_frame = QtGui.QFrame()
                diet_layout = QtGui.QVBoxLayout(diet_frame)
                diet_layout.setSpacing(5)
                diet_lbl = QtGui.QLabel('Please select all currently used food diets, if any')
                diet_lbl.setMaximumHeight(100)
                diet_lbl.setAlignment(QtCore.Qt.AlignCenter)
                diet_lbl.setStyleSheet("""font-size: 16pt;""")
                diet_grid = QtGui.QGridLayout()
                positions = [(i, j) for i in xrange(3) for j in xrange(2)]
                for diet, pos in zip(self.nut_db.metadata['diets'].iterkeys(), positions):
                    btn = SelectionButton(data={'diet': diet}, text=diet)
                    btn.setStyle(self.cleanlooks_style)
                    self.tr_buttons[1].append(btn)
                    diet_grid.addWidget(btn, *pos)
                diet_layout.addWidget(diet_lbl)
                diet_layout.addLayout(diet_grid)
                #################################################
                self.transition_btn = QtGui.QPushButton('Finish Training')
                self.transition_btn.setStyle(self.cleanlooks_style)
                self.transition_btn.setFixedSize(QtCore.QSize(150, 40))
                btn_layout = QtGui.QHBoxLayout()
                btn_layout.setSpacing(0)
                btn_layout.setMargin(0)
                btn_layout.addStretch()
                btn_layout.addWidget(self.transition_btn)
                btn_layout.addStretch()
                #################################################
                allergy_frame.setStyleSheet("""color: rgb(100, 100, 100); font-family: Calibri;""")
                diet_frame.setStyleSheet("""color: rgb(100, 100, 100); font-family: Calibri;""")
                #################################################
                work_layout.addWidget(allergy_frame)
                work_layout.addWidget(h_line)
                work_layout.addWidget(diet_frame)
                work_layout.addLayout(btn_layout)
                self.work_area.setLayout(work_layout)
                self.hl.addWidget(self.work_area, 1)

                QtCore.QObject.connect(self.transition_btn, QtCore.SIGNAL('clicked()'), self.next_tr_stage)
            else:
                if CURRENT_PROFILE.training['complete']:
                    self.universal_msg_box = MessageBox(self, 'Training Complete !')
                    self.universal_msg_box.exec_()
                    if not self.connected:
                        self.universal_msg_box = MessageBox(self, 'Cannot load data. No network connection!')
                        self.universal_msg_box.exec_()
                        self.tr_data = {}
                    else:
                        self.tr_data = self.rec_db.pull_training_recipes(self.recipes.values())
                    self.account_overview()
                    CURRENT_PROFILE.reset_training_data()
                    if 'training' not in self.tr_data:
                        CURRENT_PROFILE.training['tutorial-complete'] = True
                else:
                    if not self.tr_data and CURRENT_PROFILE.training['tutorial-complete']:
                        if not self.connected:
                            self.universal_msg_box = MessageBox(self, 'Cannot load data. No network connection!')
                            self.universal_msg_box.exec_()
                            return
                        else:
                            self.tr_data = self.rec_db.pull_training_recipes(self.recipes.values())
                    self.refresh()
                    self.tr_buttons = []
                    work_layout = QtGui.QVBoxLayout()
                    button_wrap = QtGui.QGridLayout()
                    button_wrap.setContentsMargins(20, 20, 20, 20)
                    self.transition_btn = QtGui.QPushButton('Next')
                    self.transition_btn.setStyle(self.cleanlooks_style)
                    self.transition_btn.setFixedSize(QtCore.QSize(150, 50))
                    btn_layout = QtGui.QHBoxLayout()
                    btn_layout.setSpacing(0)
                    btn_layout.setMargin(0)
                    btn_layout.addStretch()
                    btn_layout.addWidget(self.transition_btn)
                    btn_layout.addStretch()

                    start = (CURRENT_PROFILE.training['stage'] - 1) * 9
                    page_results = self.tr_data['matches'][start:start + 9]
                    positions = [(i, j) for i in xrange(3) for j in xrange(3)]
                    for position, data, image in zip(positions, page_results, xrange(start, start + 9)):
                        button_frame = QtGui.QFrame()
                        button_layout = QtGui.QHBoxLayout(button_frame)
                        button = TrainingButton(parent=button_frame, data=data, uid=image)
                        self.tr_buttons.append(button)

                        if CURRENT_PROFILE.training['tutorial-complete']:
                            if self.connected:
                                if data['smallImageUrls'][-1]:
                                    img = urlopen(str(data['smallImageUrls'][-1][:-4])).read()
                                    qp = QtGui.QPixmap()
                                    qp.loadFromData(img)
                                else:
                                    qp = QtGui.QPixmap(join(ICONS_DIR, 'no_image.png'))
                            else:
                                qp = QtGui.QPixmap(join(ICONS_DIR, 'no_image.png'))
                        else:
                            qp = QtGui.QPixmap(join(dirname(dirname(__file__)), 'training_data',
                                                    'img{0}.jpg'.format(image)))

                        if qp.width() < 512:
                            button.setIcon(QtGui.QIcon(qp.scaledToWidth(512)))
                        else:
                            button.setIcon(QtGui.QIcon(qp))
                        button.setIconSize(QtCore.QSize(512, 341))
                        button_layout.addWidget(button)
                        button_layout.setContentsMargins(0, 0, 0, 0)
                        button_wrap.addWidget(button_frame, *position)

                    work_layout.addLayout(button_wrap)
                    work_layout.addLayout(btn_layout)
                    self.work_area.setLayout(work_layout)
                    self.hl.addWidget(self.work_area, 1)

                    QtCore.QObject.connect(self.transition_btn, QtCore.SIGNAL('clicked()'), self.next_tr_stage)

    def ingredient_compound_details(self):
        self.refresh()
        work_layout = QtGui.QVBoxLayout()
        lbl = QtGui.QLabel('Use search box below to find more details about an ingredient')
        lbl.setStyleSheet("""color: rgb(100, 100, 100); font-size: 18pt; font-family: Calibri;""")
        lbl.setAlignment(QtCore.Qt.AlignHCenter)
        search_layout = QtGui.QHBoxLayout()
        search_layout.setMargin(0)
        search_layout.setSpacing(0)
        self.search_box = QtGui.QLineEdit()
        self.search_box.setFixedWidth(300)
        self.search_box.setAlignment(QtCore.Qt.AlignHCenter)
        completer = QtGui.QCompleter()
        self.search_box.setCompleter(completer)
        search_model = QtGui.QStringListModel()
        completer.setModel(search_model)
        search_model.setStringList(self.ings)
        search_btn = QtGui.QPushButton('Search!')
        search_btn.setFixedSize(QtCore.QSize(80, 40))

        logo = QtGui.QLabel()
        logo.setPixmap(QtGui.QPixmap(join(ICONS_DIR, 'login_logo.png')))
        logo.setAlignment(QtCore.Qt.AlignHCenter)
        search_layout.addWidget(self.search_box)
        search_layout.setAlignment(QtCore.Qt.AlignHCenter)
        btn_layout = QtGui.QHBoxLayout()
        btn_layout.setSpacing(0)
        btn_layout.setMargin(0)
        btn_layout.addStretch()
        btn_layout.addWidget(search_btn)
        btn_layout.addStretch()
        work_layout.addWidget(lbl)
        work_layout.addLayout(search_layout)
        work_layout.addLayout(btn_layout)
        work_layout.addWidget(logo)
        work_layout.setSpacing(30)
        work_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.work_area.setLayout(work_layout)
        self.hl.addWidget(self.work_area, 1)

        QtCore.QObject.connect(search_btn, QtCore.SIGNAL('clicked()'), self.recipe_compound_details)

    def recipe_compound_details(self):
        sender = self.sender()

        if isinstance(sender, DetailsButton):
            recipe_data = sender.data
            recipe_details = True
        else:
            if self.search_box.text() not in self.ings:
                self.universal_msg_box = MessageBox(self, 'Please select suggestion from the list. Thank you')
                self.universal_msg_box.exec_()
                return
            else:
                recipe_data = {
                    'title': self.search_box.text(),
                    'ingredients': [self.search_box.text()]
                }
                recipe_details = False
                self.refresh()

        compounds = self.nut_db.analyze_recipe_ings(recipe_data['ingredients'])
        compounds_count = 0
        for cmpd_list in compounds.itervalues():
            compounds_count += len(cmpd_list)

        work_layout = QtGui.QVBoxLayout()
        ########################################
        rec_details_frame = QtGui.QFrame()
        rec_details_frame.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Maximum))
        rec_details_frame.setStyleSheet(""".QFrame {border: 1px solid rgb(160, 160, 160); border-radius: 5px;}
        .QLabel {font-size: 18px; color: rgb(120, 120, 120);}""")
        rec_details_layout = QtGui.QGridLayout(rec_details_frame)
        rec_details_layout.setSpacing(10)
        title = QtGui.QLabel('<b>Title:</b> {0}'.format(recipe_data['title']))
        ingredients_total = QtGui.QLabel('<b>No. of Ingredients:</b> {0}'.format(len(recipe_data['ingredients'])))
        compounds_total = QtGui.QLabel('<b>No. of Compounds:</b> {0}'.format(compounds_count))

        rec_details_layout.addWidget(title, 0, 0)
        rec_details_layout.addWidget(ingredients_total, 1, 0)
        rec_details_layout.addWidget(compounds_total, 0, 1)
        ########################################
        if recipe_details:
            flavours_frame = QtGui.QFrame()
            flavours_frame.setContentsMargins(0, 0, 0, 0)
            flavours_frame.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Maximum))
            flavours_frame.setStyleSheet("""font-size: 16px; color: rgb(120, 120, 120);""")
            flavours_layout = QtGui.QHBoxLayout(flavours_frame)
            flavours_layout.setMargin(0)

            for flavour in recipe_data['flavours'].iteritems():
                flavour_frame = QtGui.QFrame()
                flavour_frame.setStyleSheet(""".QFrame {border: 1px solid rgb(160, 160, 160); border-radius: 5px;}""")
                flavour_layout = QtGui.QHBoxLayout(flavour_frame)
                flavour_name = QtGui.QLabel(flavour[0])
                flavor_bar = QtGui.QFrame()
                flavor_bar.setFixedWidth(60)
                bar_layout = QtGui.QHBoxLayout(flavor_bar)
                bar_layout.setMargin(0)
                bar_layout.setAlignment(QtCore.Qt.AlignLeft)
                bar = QtGui.QFrame()
                bar.setAutoFillBackground(True)
                bar.setStyleSheet("""background-color: rgb(230, 115, 0); border: None;""")
                bar.setFixedWidth(flavor_bar.width() * flavour[1])
                bar_layout.addWidget(bar)

                flavour_layout.addWidget(flavour_name)
                flavour_layout.addWidget(flavor_bar)
                flavours_layout.addWidget(flavour_frame)
        else:
            flavours_frame = None
        ########################################
        scroll_widget = QtGui.QWidget()
        scroll_widget.setStyleSheet(""".QFrame {border: 1px solid rgb(160, 160, 160); border-radius: 5px}""")
        scroll_layout = QtGui.QVBoxLayout(scroll_widget)
        scroll_area = QtGui.QScrollArea()
        scroll_area.setFrameShape(QtGui.QFrame.NoFrame)
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(scroll_widget)
        for ing, cmpds in compounds.iteritems():
            cmpd_names = self.nut_db.get_compound_data(cmpds)
            cmpd_flavours = self.nut_db.get_flavour_data(cmpds)
            cmpd_he = self.nut_db.get_health_effects_data(cmpds)

            ing_frame = QtGui.QFrame()
            ing_frame.setStyleSheet("""color: rgb(120, 120, 120);""")
            ing_layout = QtGui.QVBoxLayout(ing_frame)
            ing_layout.setSpacing(10)
            ing_lbl = QtGui.QLabel(str(ing).capitalize())
            ing_lbl.setStyleSheet("""font-weight: bold; font-size: 16px;""")
            ing_layout.addWidget(ing_lbl)
            ###################################
            cmpd_widget = QtGui.QWidget()
            cmpd_layout = QtGui.QVBoxLayout(cmpd_widget)
            cmpd_scroll = QtGui.QScrollArea()
            if len(cmpds) > 4 and recipe_details:
                cmpd_scroll.setFixedHeight(300)
            cmpd_scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
            cmpd_scroll.setWidgetResizable(True)
            cmpd_scroll.setWidget(cmpd_widget)
            for cid in cmpds:
                cmpd_lbl = QtGui.QLabel(
                    '<b style="font-size: 15px;">{0}</b> <b>flavours:</b> {1}, <b>biological activity:</b> {2}'.format
                    (cmpd_names[cid], ', '.join(cmpd_flavours[cid]), ', '.join(cmpd_he[cid])))
                cmpd_lbl.setWordWrap(True)
                cmpd_layout.addWidget(cmpd_lbl)
            if not cmpds:
                cmpd_lbl = QtGui.QLabel('<b>No compounds found!</b>')
                cmpd_lbl.setWordWrap(True)
                cmpd_layout.addWidget(cmpd_lbl)

            ing_layout.addWidget(cmpd_scroll)
            scroll_layout.addWidget(ing_frame)
        ########################################
        work_layout.addWidget(rec_details_frame)
        if recipe_details:
            work_layout.addWidget(flavours_frame)
        work_layout.addWidget(scroll_area)
        work_layout.setAlignment(QtCore.Qt.AlignTop)
        ########################################
        if recipe_details:
            self.compound_widget = QtGui.QWidget()
            self.compound_widget.setWindowIcon(QtGui.QIcon(QtGui.QPixmap(join(ICONS_DIR, 'icon.png'))))
            self.compound_widget.setVisible(True)
            self.compound_widget.setWindowTitle('Compound decomposition of {0}'.format(recipe_data['title']))
            self.compound_widget.setLayout(work_layout)
            resize(self.compound_widget, 900, 800)
            center(self.compound_widget)
        else:
            self.work_area.setLayout(work_layout)
            self.hl.addWidget(self.work_area, 1)

    @staticmethod
    def get_fav_state(uid):
        if uid in CURRENT_PROFILE.favourite['recipes']:
            return True
        return False

    def recipe_search(self):
        self.refresh()
        work_layout = QtGui.QVBoxLayout()
        lbl = QtGui.QLabel('Use search box below to find your favourite course')
        lbl.setStyleSheet("""color: rgb(100, 100, 100); font-size: 18pt; font-family: Calibri;""")
        lbl.setAlignment(QtCore.Qt.AlignHCenter)
        search_layout = QtGui.QHBoxLayout()
        search_layout.setMargin(0)
        search_layout.setSpacing(0)
        self.search_box = QtGui.QLineEdit()
        self.search_box.setFixedWidth(400)
        self.search_box.setAlignment(QtCore.Qt.AlignHCenter)
        completer = QtGui.QCompleter()
        self.search_box.setCompleter(completer)
        search_model = QtGui.QStringListModel()
        completer.setModel(search_model)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        search_model.setStringList(self.recipes.keys())
        search_btn = QtGui.QPushButton('Search!')
        search_btn.setFixedSize(QtCore.QSize(80, 40))

        logo = QtGui.QLabel()
        logo.setPixmap(QtGui.QPixmap(join(ICONS_DIR, 'login_logo.png')))
        logo.setAlignment(QtCore.Qt.AlignHCenter)
        search_layout.addWidget(self.search_box)
        search_layout.setAlignment(QtCore.Qt.AlignHCenter)
        btn_layout = QtGui.QHBoxLayout()
        btn_layout.setSpacing(0)
        btn_layout.setMargin(0)
        btn_layout.addStretch()
        btn_layout.addWidget(search_btn)
        btn_layout.addStretch()
        work_layout.addWidget(lbl)
        work_layout.addLayout(search_layout)
        work_layout.addLayout(btn_layout)
        work_layout.addWidget(logo)
        work_layout.setSpacing(30)
        work_layout.setAlignment(QtCore.Qt.AlignCenter)
        self.work_area.setLayout(work_layout)
        self.hl.addWidget(self.work_area, 1)

        QtCore.QObject.connect(search_btn, QtCore.SIGNAL('clicked()'), self.recipe_details)

    def recipe_details(self):
        self.connected = Utils.check_connection()
        sender = self.sender()

        if isinstance(sender, QtGui.QLabel):
            recipe = self.yummly.recipe_details(sender['uid'])
        elif isinstance(sender, DetailsButton):
            recipe = self.yummly.recipe_details(sender['data'])
        else:
            if self.search_box.text() not in self.recipes.keys():
                self.universal_msg_box = MessageBox(self, 'Please select suggestion from the list. Thank you')
                self.universal_msg_box.exec_()
                return
            else:
                if not self.connected:
                    self.universal_msg_box = MessageBox(self, 'Cannot load data. No network connection!')
                    self.universal_msg_box.exec_()
                    return
                recipe = self.yummly.recipe_details(self.recipes[str(self.search_box.text())])
                self.refresh()

        work_layout = QtGui.QVBoxLayout()
        #################################
        top_frame = QtGui.QFrame()
        top_frame.setMaximumHeight(300)
        top_layout = QtGui.QHBoxLayout()
        top_frame.setContentsMargins(0, 0, 0, 0)
        top_frame.setContentsMargins(0, 0, 0, 0)
        top_layout.setMargin(0)
        top_layout.setSpacing(0)
        img_layout = QtGui.QVBoxLayout()
        basic_desc_layout = QtGui.QVBoxLayout()
        basic_desc_layout.setMargin(25)
        basic_desc_layout.setSpacing(25)
        btn_layout = QtGui.QHBoxLayout()
        img_lbl = QtGui.QLabel()
        img_lbl.setStyleSheet("""border: 2px solid rgb(200, 200, 200);""")
        title_lbl = QtGui.QLabel('<b>' + str(recipe['name']) + '</b>')
        title_lbl.setAlignment(QtCore.Qt.AlignHCenter)
        title_lbl.setWordWrap(True)
        title_lbl.setMargin(10)
        title_lbl.setStyleSheet("""background-color: rgb(230, 230, 230); font-size: 11pt; font-family: Calibri;""")
        if not recipe['totalTime']:
            recipe['totalTime'] = 'Uknown'
        cook_time_lbl = QtGui.QLabel("<b>Cooking Time: </b>" + recipe['totalTime'])
        cook_time_lbl.setAlignment(QtCore.Qt.AlignHCenter)
        cook_time_lbl.setStyleSheet("""background-color: rgb(230, 230, 230); font-size: 11pt; font-family: Calibri;""")
        cook_time_lbl.setMargin(10)
        servings_lbl = QtGui.QLabel("<b>Number of servings: </b>" + str(recipe['numberOfServings']))
        servings_lbl.setAlignment(QtCore.Qt.AlignHCenter)
        servings_lbl.setStyleSheet("""background-color: rgb(230, 230, 230); font-size: 11pt; font-family: Calibri;""")
        servings_lbl.setMargin(10)

        #################################
        favourite_btn = PushButton((recipe['name'], recipe['id']), 'fav_sel.png', 'fav_desel.png',
                                   self.get_fav_state((recipe['name'], recipe['id'])))
        favourite_btn.setIconSize(QtCore.QSize(30, 30))
        favourite_btn.setFixedHeight(40)
        details_btn = DetailsButton(data={
            'title': recipe['name'],
            'ingredients': recipe['ingredientLines'],
            'flavours': recipe['flavors']
        })
        #################################
        bottom_frame = QtGui.QFrame()
        bottom_layout = QtGui.QHBoxLayout()
        nutrient_scroll = QtGui.QScrollArea()
        nutrient_scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        nutrient_scroll.setWidgetResizable(True)
        nutrient_widget = QtGui.QWidget()
        nutrient_layout = QtGui.QVBoxLayout(nutrient_widget)
        nut_frame = QtGui.QFrame()
        nut_frame.setStyleSheet("""color: rgb(210, 105, 0); font-weight: bold""")
        nut_frame.setContentsMargins(10, 0, 0, 0)
        nut_frame.setFixedHeight(75)
        nut_layout = QtGui.QHBoxLayout(nut_frame)
        nutrient_lbl = QtGui.QLabel('NUTRIENTS')
        weight_lbl = QtGui.QLabel('')
        weight_lbl.setFixedWidth(100)
        gda_m_lbl = QtGui.QLabel('M')
        gda_m_lbl.setFixedWidth(50)
        gda_m_lbl.setAlignment(QtCore.Qt.AlignCenter)
        gda_f_lbl = QtGui.QLabel('F')
        gda_f_lbl.setFixedWidth(50)
        gda_f_lbl.setAlignment(QtCore.Qt.AlignCenter)
        nut_layout.addWidget(nutrient_lbl)
        nut_layout.addWidget(weight_lbl)
        nut_layout.addWidget(gda_m_lbl)
        nut_layout.addWidget(gda_f_lbl)
        ingredient_scroll = QtGui.QScrollArea()
        ingredient_scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        ingredient_scroll.setWidgetResizable(True)
        ingredient_widget = QtGui.QWidget()
        ingredient_layout = QtGui.QVBoxLayout(ingredient_widget)
        ingredient_lbl = QtGui.QLabel('<b>INGREDIENTS</b>')
        ingredient_lbl.setFixedHeight(75)
        ingredient_lbl.setContentsMargins(10, 0, 0, 0)
        ingredient_lbl.setStyleSheet("""color: rgb(210, 105, 0);""")

        img_lbl.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        if self.connected:
            if recipe['images'][-1]['hostedSmallUrl']:
                img = urlopen(str(recipe['images'][0]['hostedSmallUrl'][:-4])).read()
                qp = QtGui.QPixmap()
                qp.loadFromData(img)
            else:
                qp = QtGui.QPixmap(join(ICONS_DIR, 'no_image.png'))
        else:
            qp = QtGui.QPixmap(join(ICONS_DIR, 'no_image.png'))
        img_lbl.setPixmap(qp.scaledToHeight(300))
        img_lbl.setAlignment(QtCore.Qt.AlignHCenter)
        img_layout.addWidget(img_lbl)

        btn_layout.addWidget(favourite_btn)
        btn_layout.addWidget(details_btn)

        basic_desc_layout.setAlignment(QtCore.Qt.AlignVCenter)
        basic_desc_layout.addWidget(title_lbl)
        basic_desc_layout.addWidget(cook_time_lbl)
        basic_desc_layout.addWidget(servings_lbl)
        basic_desc_layout.addLayout(btn_layout)
        #####################################
        top_layout.addLayout(img_layout)
        top_layout.addLayout(basic_desc_layout)
        #####################################
        nutrient_layout.addWidget(nut_frame)
        nuts = {n['attribute']: n for n in recipe['nutritionEstimates']
                if n['attribute'] in self.yummly.important_nutrients}
        if nuts:
            i = 0
            for nut in self.yummly.important_nutrients:
                if nut in nuts:
                    nut_frame = QtGui.QFrame()
                    nut_frame.setContentsMargins(0, 0, 0, 0)
                    nut_frame.setFixedHeight(50)
                    nut_layout = QtGui.QHBoxLayout(nut_frame)
                    if nuts[nut]['attribute'] == 'VITA_RAE':
                        nut_lbl = QtGui.QLabel('Vitamin A, Retinol')
                    elif nuts[nut]['attribute'] == 'VITA_IU':
                        nut_lbl = QtGui.QLabel('Vitamin A, Beta Carotene')
                    elif nuts[nut]['attribute'] == 'FAT':
                        nut_lbl = QtGui.QLabel('Total Fat')
                    elif nuts[nut]['attribute'] == 'FASAT':
                        nut_lbl = QtGui.QLabel('Fat (Saturated)')
                    elif nuts[nut]['attribute'] == 'CHOCDF':
                        nut_lbl = QtGui.QLabel('Carbohydrates')
                    else:
                        nut_lbl = QtGui.QLabel(re_split(', | \(| \+', nuts[nut]['description'])[0])
                    nut_lbl.setWordWrap(True)
                    nut_layout.addWidget(nut_lbl)
                    weight = QtGui.QLabel('{0} {1}'.format(*Utils.convert_weight(
                        nuts[nut]['attribute'], nuts[nut]['value'], nuts[nut]['unit']['abbreviation'])))
                    weight.setFixedWidth(100)
                    nut_layout.addWidget(weight)
                    gda_m = QtGui.QLabel(str(Utils.get_rda('male', nuts[nut]['attribute'], nuts[nut]['value'])))
                    gda_m.setAlignment(QtCore.Qt.AlignCenter)
                    gda_m.setFixedWidth(50)
                    gda_f = QtGui.QLabel(str(Utils.get_rda('female', nuts[nut]['attribute'], nuts[nut]['value'])))
                    gda_f.setAlignment(QtCore.Qt.AlignCenter)
                    gda_f.setFixedWidth(50)
                    nut_layout.addWidget(gda_m)
                    nut_layout.addWidget(gda_f)
                    if i % 2 == 1:
                        nut_frame.setStyleSheet("""background-color: rgb(230, 230, 230);
                        font-size: 9pt; font-family: Calibri;""")
                    else:
                        nut_frame.setStyleSheet("""background-color: rgb(220, 220, 220);
                        font-size: 9pt; font-family: Calibri;""")
                    nut_lbl.setContentsMargins(25, 0, 0, 0)
                    nutrient_layout.addWidget(nut_frame)
                    i += 1
        else:
            nut_frame = QtGui.QFrame()
            nut_layout = QtGui.QHBoxLayout(nut_frame)
            nut_lbl = QtGui.QLabel('No nutrients found!')
            nut_layout.addWidget(nut_lbl)
            nut_frame.setStyleSheet("""background-color: rgb(220, 220, 220);
                    font-size: 9pt; font-family: Calibri;""")
            nutrient_layout.addWidget(nut_frame)

        nutrient_layout.setAlignment(QtCore.Qt.AlignTop)
        #####################################
        ingredient_layout.addWidget(ingredient_lbl)
        for i, ing in enumerate(recipe['ingredientLines']):
            ing_lbl = QtGui.QLabel(ing)
            ing_lbl.setWordWrap(True)
            ing_lbl.setFixedHeight(75)
            if i % 2 == 1:
                ing_lbl.setStyleSheet("""background-color: rgb(230, 230, 230);
                font-size: 10pt; font-family: Calibri;""")
            else:
                ing_lbl.setStyleSheet("""background-color: rgb(220, 220, 220);
                font-size: 10pt; font-family: Calibri;""")
            ing_lbl.setContentsMargins(25, 0, 0, 0)
            ingredient_layout.addWidget(ing_lbl)
        ingredient_layout.setAlignment(QtCore.Qt.AlignTop)
        #####################################
        nutrient_scroll.setWidget(nutrient_widget)
        ingredient_scroll.setWidget(ingredient_widget)
        bottom_layout.addWidget(nutrient_scroll)
        bottom_layout.addWidget(ingredient_scroll)
        #####################################
        top_frame.setStyleSheet("""
        .QFrame {border: 2px solid rgb(210, 210, 210);}
        .QLabel {color: rgb(100, 100, 100); font-size: 16pt; font-family: Calibri;}""")
        bottom_frame.setStyleSheet("""color: rgb(100, 100, 100); font-size: 16pt; font-family: Calibri;""")
        top_frame.setLayout(top_layout)
        bottom_frame.setLayout(bottom_layout)
        work_layout.addWidget(top_frame)
        work_layout.addWidget(bottom_frame)
        #####################################
        QtCore.QObject.connect(details_btn, QtCore.SIGNAL('clicked()'), self.recipe_compound_details)
        #####################################
        if isinstance(sender, QtGui.QLabel) or isinstance(sender, DetailsButton):
            self.details_widget = QtGui.QWidget()
            self.details_widget.setWindowIcon(QtGui.QIcon(QtGui.QPixmap(join(ICONS_DIR, 'icon.png'))))
            self.details_widget.setVisible(True)
            self.details_widget.setWindowTitle(recipe['name'])
            self.details_widget.setLayout(work_layout)
            resize(self.details_widget, 900, 800)
            center(self.details_widget)
        else:
            self.work_area.setLayout(work_layout)
            self.hl.addWidget(self.work_area, 1)

    def account_overview(self):
        self.connected = Utils.check_connection()
        self.refresh()
        work_layout = QtGui.QVBoxLayout()
        content_frame = QtGui.QFrame()
        content_frame.setStyleSheet("""font-size: 14px; color: rgb(120, 120, 120);""")
        content_frame.setContentsMargins(0, 0, 0, 0)
        content_layout = QtGui.QVBoxLayout(content_frame)
        content_layout.setMargin(0)
        content_layout.setSpacing(15)
        #####################################
        personal_frame = QtGui.QFrame()
        personal_frame.setMaximumHeight(125)
        personal_layout = QtGui.QVBoxLayout(personal_frame)
        details_box = QtGui.QGridLayout()
        title = QtGui.QLabel('Personal Details')
        title.setStyleSheet("""font-size: 16px; font-weight: bold;""")
        positions = [(i, j) for i in xrange(2) for j in xrange(2)]
        for lbl, pos in zip(['First name', 'Gender', 'Last name', 'Age'], positions):
            detail_frame = QtGui.QFrame()
            detail_layout = QtGui.QHBoxLayout(detail_frame)
            detail_lbl = QtGui.QLabel('{0}:'.format(lbl))
            detail_lbl.setFixedWidth(100)
            detail_lbl.setAlignment(QtCore.Qt.AlignCenter)
            detail_layout.addWidget(detail_lbl)
            if pos[1] % 2 == 0:
                detail_edit = QtGui.QLineEdit(str(CURRENT_PROFILE.personal_details[lbl.lower()]))
            else:
                detail_edit = QtGui.QComboBox()
                if pos[0] == 0:
                    detail_edit.addItem('Male')
                    detail_edit.addItem('Female')
                else:
                    for num in xrange(10, 100):
                        detail_edit.addItem(str(num))
                detail_edit.setCurrentIndex(detail_edit.findText(
                    str(CURRENT_PROFILE.personal_details[lbl.lower()])))
            detail_edit.setFixedHeight(25)
            detail_layout.addWidget(detail_edit)
            details_box.addWidget(detail_frame, *pos)
        personal_layout.addWidget(title)
        personal_layout.addLayout(details_box)
        personal_layout.setAlignment(QtCore.Qt.AlignTop)
        #####################################
        h_line1 = QtGui.QFrame()
        h_line1.setFrameShape(QtGui.QFrame.HLine)
        h_line1.setFrameShadow(QtGui.QFrame.Sunken)
        #####################################
        flavours_frame = QtGui.QFrame()
        flavours_frame.setContentsMargins(0, 0, 0, 0)
        flavours_frame.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Maximum))

        flavours_layout = QtGui.QHBoxLayout(flavours_frame)
        flavours_layout.setMargin(0)

        for flavour in CURRENT_PROFILE.preferences['flavour_attrs'].iteritems():
            flavour_frame = QtGui.QFrame()
            flavour_frame.setStyleSheet(""".QFrame {border: 1px solid rgb(160, 160, 160); border-radius: 5px;}""")
            flavour_layout = QtGui.QHBoxLayout(flavour_frame)
            flavour_name = QtGui.QLabel(flavour[0])
            flavor_bar = QtGui.QFrame()
            flavor_bar.setFixedWidth(60)
            bar_layout = QtGui.QHBoxLayout(flavor_bar)
            bar_layout.setMargin(0)
            bar_layout.setAlignment(QtCore.Qt.AlignLeft)
            bar = QtGui.QFrame()
            bar.setAutoFillBackground(True)
            bar.setStyleSheet("""background-color: rgb(230, 115, 0); border: None;""")
            bar.setFixedWidth(flavor_bar.width() * flavour[1])
            bar_layout.addWidget(bar)

            flavour_layout.addWidget(flavour_name)
            flavour_layout.addWidget(flavor_bar)
            flavours_layout.addWidget(flavour_frame)
        #####################################
        h_line2 = QtGui.QFrame()
        h_line2.setFrameShape(QtGui.QFrame.HLine)
        h_line2.setFrameShadow(QtGui.QFrame.Sunken)
        #####################################
        cuisine_frame = QtGui.QFrame()
        cuisine_frame.setMaximumHeight(75)
        cuisine_layout = QtGui.QVBoxLayout(cuisine_frame)
        title = QtGui.QLabel('Cuisines')
        title.setStyleSheet("""font-size: 16px; font-weight: bold;""")
        cuisines = QtGui.QLabel(', '.join(CURRENT_PROFILE.preferences['cuisines'].keys()))
        cuisines.setWordWrap(True)
        cuisine_layout.addWidget(title)
        cuisine_layout.addWidget(cuisines)
        cuisine_layout.setAlignment(QtCore.Qt.AlignTop)
        #####################################
        h_line3 = QtGui.QFrame()
        h_line3.setFrameShape(QtGui.QFrame.HLine)
        h_line3.setFrameShadow(QtGui.QFrame.Sunken)
        #####################################
        course_frame = QtGui.QFrame()
        course_frame.setMaximumHeight(75)
        course_layout = QtGui.QVBoxLayout(course_frame)
        title = QtGui.QLabel('Meal Types (Courses)')
        title.setStyleSheet("""font-size: 16px; font-weight: bold;""")
        courses = QtGui.QLabel(', '.join(CURRENT_PROFILE.preferences['courses'].keys()))
        courses.setWordWrap(True)
        course_layout.addWidget(title)
        course_layout.addWidget(courses)
        course_layout.setAlignment(QtCore.Qt.AlignTop)
        #####################################
        h_line4 = QtGui.QFrame()
        h_line4.setFrameShape(QtGui.QFrame.HLine)
        h_line4.setFrameShadow(QtGui.QFrame.Sunken)
        #####################################
        ad_frame = QtGui.QFrame()
        ad_frame.setMaximumHeight(75)
        ad_layout = QtGui.QHBoxLayout(ad_frame)
        allergy_layout = QtGui.QVBoxLayout()
        allergy_lbl = QtGui.QLabel('Allergies')
        allergy_lbl.setStyleSheet("""font-size: 16px; font-weight: bold;""")
        allergies = QtGui.QLabel(', '.join(CURRENT_PROFILE.preferences['allergies']))
        allergies.setWordWrap(True)
        allergy_layout.addWidget(allergy_lbl)
        allergy_layout.addWidget(allergies)
        allergy_layout.setAlignment(QtCore.Qt.AlignTop)

        v_line = QtGui.QFrame()
        v_line.setFrameShape(QtGui.QFrame.VLine)
        v_line.setFrameShadow(QtGui.QFrame.Sunken)

        diet_layout = QtGui.QVBoxLayout()
        diet_lbl = QtGui.QLabel('Diets')
        diet_lbl.setStyleSheet("""font-size: 16px; font-weight: bold;""")
        diets = QtGui.QLabel(', '.join(CURRENT_PROFILE.preferences['diets']))
        diets.setWordWrap(True)
        diet_layout.addWidget(diet_lbl)
        diet_layout.addWidget(diets)
        diet_layout.setAlignment(QtCore.Qt.AlignTop)

        ad_layout.addLayout(allergy_layout)
        ad_layout.addWidget(v_line)
        ad_layout.addLayout(diet_layout)
        #####################################
        h_line5 = QtGui.QFrame()
        h_line5.setFrameShape(QtGui.QFrame.HLine)
        h_line5.setFrameShadow(QtGui.QFrame.Sunken)
        #####################################
        fav_recipe_frame = QtGui.QFrame()
        fav_recipe_layout = QtGui.QVBoxLayout(fav_recipe_frame)
        title = QtGui.QLabel('Favourite recipes')
        title.setStyleSheet("""font-size: 16px; font-weight: bold;""")
        fav_recipe_layout.addWidget(title)
        fav_recipe_scroll = QtGui.QScrollArea()
        fav_recipe_scroll.setWidgetResizable(True)
        fav_recipe_widget = QtGui.QWidget()
        fav_recipe_scroll_layout = QtGui.QVBoxLayout(fav_recipe_widget)
        for recipe in CURRENT_PROFILE.favourite['recipes']:
            recipe_frame = QtGui.QFrame()
            recipe_layout = QtGui.QHBoxLayout(recipe_frame)
            recipe_lbl = QtGui.QLabel(recipe[0])
            recipe_btn = DetailsButton(data=recipe[1])
            recipe_btn.setStyle(self.cleanlooks_style)
            recipe_btn.setMaximumWidth(100)
            recipe_layout.addWidget(recipe_lbl)
            recipe_layout.addWidget(recipe_btn)
            fav_recipe_scroll_layout.addWidget(recipe_frame)
            QtCore.QObject.connect(recipe_btn, QtCore.SIGNAL('clicked()'), self.recipe_details)
            fav_recipe_scroll.setWidget(fav_recipe_widget)
        fav_recipe_layout.addWidget(fav_recipe_scroll)
        fav_recipe_layout.setAlignment(QtCore.Qt.AlignTop)
        #####################################
        h_line6 = QtGui.QFrame()
        h_line6.setFrameShape(QtGui.QFrame.HLine)
        h_line6.setFrameShadow(QtGui.QFrame.Sunken)
        #####################################
        opt_btn_frame = QtGui.QFrame()
        opt_btn_layout = QtGui.QHBoxLayout(opt_btn_frame)
        opt_btn_layout.addStretch()
        update_btn = QtGui.QPushButton('Update')
        update_btn.setStyle(self.cleanlooks_style)
        update_btn.setFixedSize(QtCore.QSize(80, 40))
        opt_btn_layout.addWidget(update_btn)
        opt_btn_layout.addStretch()
        QtCore.QObject.connect(update_btn, QtCore.SIGNAL('clicked()'), self.update_account)
        #####################################
        content_layout.addWidget(personal_frame)
        content_layout.addWidget(h_line1)
        content_layout.addWidget(flavours_frame)
        content_layout.addWidget(h_line2)
        content_layout.addWidget(cuisine_frame)
        content_layout.addWidget(h_line3)
        content_layout.addWidget(course_frame)
        content_layout.addWidget(h_line4)
        content_layout.addWidget(ad_frame)
        content_layout.addWidget(h_line5)
        content_layout.addWidget(fav_recipe_frame)
        content_layout.addWidget(h_line6)
        content_layout.addWidget(opt_btn_frame)
        #####################################
        work_layout.addWidget(content_frame)
        self.work_area.setLayout(work_layout)
        self.hl.addWidget(self.work_area, 1)

    def update_account(self):
        positions = [(i, j) for i in xrange(2) for j in xrange(2)]
        for lbl, pos in zip(['First name', 'Gender', 'Last name', 'Age'], positions):
            item = self.sender().parent().parent().layout().itemAt(0).widget().layout().itemAt(1)\
                .itemAtPosition(*pos).widget().layout().itemAt(1).widget()
            if isinstance(item, QtGui.QLineEdit):
                CURRENT_PROFILE.personal_details[lbl.lower()] = item.text()
            else:
                CURRENT_PROFILE.personal_details[lbl.lower()] = item.currentText()
        self.account_overview()

    def start_worker(self):
        self.worker = BackgroundThread(self.rec_db.make_recommendations, CURRENT_PROFILE)
        QtCore.QObject.connect(self.worker, QtCore.SIGNAL('finished(PyQt_PyObject)'), self.set_recommendations)
        self.worker.start()

    def set_recommendations(self, r):
        CURRENT_PROFILE.set_recommendations(r[1])
        self.loading_box.finish(True)
        self.recommendations()


class TrainingButton(QtGui.QPushButton):
    def __init__(self, parent=None, uid=None, data=None, text=None):
        super(TrainingButton, self).__init__(parent)
        self.setMouseTracking(True)
        if text:
            self.setText(text)
        self.uid = uid
        self.data = data
        self.selected = False

    def mousePressEvent(self, event):
        if self.selected:
            self.selected = False
            self.parentWidget().setStyleSheet(""".QFrame {border: None;}""")
        else:
            self.selected = True
            self.parentWidget().setStyleSheet(""".QFrame {border: 5px solid rgb(255, 150, 50);}""")


class SelectionButton(QtGui.QPushButton):
    def __init__(self, parent=None, uid=None, data=None, text=None):
        super(SelectionButton, self).__init__(parent)
        self.setMouseTracking(True)
        if text:
            self.setText(text)
        self.uid = uid
        self.data = data
        self.selected = False

    def mousePressEvent(self, event):
        if self.selected:
            self.selected = False
            self.setStyleSheet("""""")
        else:
            self.selected = True
            self.setStyleSheet("""QPushButton {background-color: rgb(175, 0, 0); color: white;
            border: 1px solid rgb(160, 160, 160); border-radius: 5px; padding: 3px;}""")


class PushButton(QtGui.QPushButton):
    def __init__(self, uid, sel_icon, desel_icon, selected=False, parent=None):
        super(PushButton, self).__init__(parent)
        self.setMouseTracking(True)
        self.selected = selected
        self.s_icon = sel_icon
        self.d_icon = desel_icon
        self.uid = uid
        self.select_icon()

    def select_icon(self):
        if self.selected:
            self.setIcon(QtGui.QIcon(QtGui.QPixmap(join(ICONS_DIR, self.s_icon))))
        else:
            self.setIcon(QtGui.QIcon(QtGui.QPixmap(join(ICONS_DIR, self.d_icon))))

    def mousePressEvent(self, event):
        if self.selected:
            self.selected = False
            CURRENT_PROFILE.remove_fav_recipe(self.uid)
        else:
            self.selected = True
            CURRENT_PROFILE.add_fav_recipe(self.uid)
        self.select_icon()


class ImageLabel(QtGui.QLabel):
    def __init__(self, uid, *__args):
        QtGui.QLabel.__init__(self, *__args)
        self.uid = uid

    def mousePressEvent(self, event):
        QtCore.QObject.emit(self, QtCore.SIGNAL('clicked()'))

    def __getitem__(self, item):
        return getattr(self, item)


class DetailsButton(QtGui.QPushButton):
    def __init__(self, parent=None, data=None, *__args):
        super(DetailsButton, self).__init__(parent, *__args)
        self.data = data
        self.setIcon(QtGui.QIcon(QtGui.QPixmap(join(ICONS_DIR, 'details.png'))))
        self.setIconSize(QtCore.QSize(60, 40))
        self.setFixedHeight(40)

    def __getitem__(self, item):
        return getattr(self, item)


class MessageBox(QtGui.QMessageBox):

    def __init__(self, parent=None, text=None, t=QtGui.QMessageBox.Information, *args):
        super(MessageBox, self).__init__(parent, *args)
        self.setStyle(QtGui.QStyleFactory().create('cleanlooks'))
        self.setStyleSheet("""font-size: 14px; color: rgb(120, 120, 120); font-family: Calibri;""")
        self.setIcon(t)
        if t == QtGui.QMessageBox.Question:
            self.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
            self.setDefaultButton(QtGui.QMessageBox.Yes)
        if text:
            self.setText(text)


class ProcessingBox(QtGui.QMessageBox):

    def __init__(self, parent=None):
        super(ProcessingBox, self).__init__(parent)
        self.stop = False
        self.setStyleSheet("""font-size: 16px; color: rgb(120, 120, 120); font-family: Calibri;""")
        self.lbl = QtGui.QLabel()
        self.lbl.setContentsMargins(10, 0, 10, 0)
        mov = QtGui.QMovie(join(ICONS_DIR, 'loading.gif'))
        mov.setScaledSize(QtCore.QSize(60, 60))
        mov.start()
        self.lbl.setMovie(mov)
        self.layout().addWidget(self.lbl, 0, 2)
        self.setText('Processing')

    def finish(self, b=True):
        self.stop = b

    def showEvent(self, e):
        self.startTimer(10)

    def timerEvent(self, e):
        if self.stop:
            self.done(0)


class BackgroundThread(QThread):

    def __init__(self, function, *args, **kwargs):
        QThread.__init__(self)
        self.function = function
        self.args = args
        self.kwargs = kwargs

    def __del__(self):
        self.wait()

    def run(self):
        out = self.function(*self.args, **self.kwargs)
        self.emit(QtCore.SIGNAL('finished(PyQt_PyObject)'), out)
