#! /usr/bin/python

from __future__ import division
from GUI import LoginForm, MainWindow, set_profile, MessageBox
from sys import argv
from PyQt4 import QtCore, QtGui
import User

"""
    Dishifier is a core, fundamental class that defines and runs dishifier
    application.
"""


class Dishifier:

    def __init__(self):
        self.login_form = LoginForm()
        self.main_window = MainWindow()
        self.msg_box = MessageBox(self.login_form)
        QtCore.QObject.connect(self.login_form.login_button, QtCore.SIGNAL("clicked()"), self.verify_user)
        QtCore.QObject.connect(self.main_window.logout_icon, QtCore.SIGNAL("clicked()"), self.logout)

    def logout(self):
        self.msg_box = MessageBox(self.login_form, 'Are you sure to logout?', QtGui.QMessageBox.Question)
        reply = self.msg_box.exec_()
        if reply == QtGui.QMessageBox.Yes:
            self.main_window.hide()
            self.main_window = MainWindow()
            self.login_form.show()

    def verify_user(self):
        usr = self.login_form.login_box.text()
        self.login_form.login_box.clear()

        if usr:
            p, msg = User.user_login(usr)
            if p:
                self.msg_box = MessageBox(self.login_form, msg)
                self.msg_box.exec_()
                set_profile(p)
                if p.training['tutorial-complete']:
                    self.main_window.tr_data = self.main_window.rec_db.pull_training_recipes(
                        self.main_window.recipes.values())
                else:
                    self.main_window.tr_data = User.load_training_data()
                self.login_form.hide()
                self.main_window.account_overview()
                self.main_window.show()
            else:
                self.msg_box = MessageBox(self.login_form, msg, QtGui.QMessageBox.Warning)
                self.msg_box.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                reply = self.msg_box.exec_()
                if reply == QtGui.QMessageBox.Yes:
                    p = User.Profile(usr)
                    set_profile(p)
                    self.msg_box = MessageBox(self.login_form, 'Profile successfully created !')
                    self.msg_box.exec_()
                    if p.training['tutorial-complete']:
                        self.main_window.tr_data = self.main_window.rec_db.pull_training_recipes()
                    else:
                        self.main_window.tr_data = User.load_training_data()
                    self.login_form.hide()
                    self.main_window.training_system()
                    self.main_window.show()

if __name__ == "__main__":
    try:
        app = QtGui.QApplication(argv)
        d = Dishifier()
        exit(app.exec_())
    except (KeyboardInterrupt, EOFError):
        print ('\n')
        print '*' * 60
        print ('{:*^60}'.format(' Exiting gracefully now! '))
        print '*' * 60
