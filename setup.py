from setuptools import setup

setup(
	name = 'dishifier',

	version = '1.0',
	
	description = 'Python package to categorize dish\'s cuisine based on ingredients',
	
	# Author details
	author = 'bdk',
	author_email = 'bdk@aber.ac.uk',
	
	# License details
	license='MIT', requires=['flask', 'sqlalchemy', 'requests', 'nltk', 'PyQt4', 'sip', 'numpy', 'sklearn']
)
