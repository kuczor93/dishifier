#!/usr/bin/env python

from os import system as os_system, getcwd, chdir
from os.path import join

# install pip if not installed
os_system('python {0}'.format(join(getcwd(), 'get-pip.py')))

# install requirements
os_system('pip install -r {0}'.format(join(getcwd(), 'requirements.txt')))

# install pyqt4 and sip
cur_dir = getcwd()
os_system('sudo python {0}'.format(join(getcwd(), 'sip-4.17/configure.py')))
chdir(join(getcwd(), 'sip-4.17'))
os_system('make')
os_system('make install')
chdir(cur_dir)

os_system('sudo python {0}'.format(join(getcwd(), 'PyQt-x11-gpl-4.11.4/configure.py')))
chdir(join(getcwd(), 'PyQt-x11-gpl-4.11.4'))
os_system('make')
os_system('make install')
chdir(cur_dir)

